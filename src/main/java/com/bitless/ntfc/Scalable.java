package com.bitless.ntfc;

/**
 *
 * @author pavulzavala
 */
public interface Scalable 
{
    
    public void scale( float x, float y);
    
}
