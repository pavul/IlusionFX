/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bitless.ntfc;

import javafx.scene.canvas.GraphicsContext;

/**
 * @author PavulZavala
 */
public interface PostDrawable 
{
    public void postDraw( GraphicsContext g );
}//
