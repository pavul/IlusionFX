package com.bitless.ntfc;

/**
 *
 * @author pavulzavala
 */
@FunctionalInterface
public interface Startable 
{
public void startGame();
}
