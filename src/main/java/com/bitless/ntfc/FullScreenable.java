package com.bitless.ntfc;

/**
 * this interface it can be used to
 * make the game full screen
 * @author pavulzavala
 */
@FunctionalInterface
public interface FullScreenable 
{
    public void setFullScreen();
}
