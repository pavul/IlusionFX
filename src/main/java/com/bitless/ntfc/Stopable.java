package com.bitless.ntfc;

/**
 *
 * @author pavulzavala
 */
@FunctionalInterface
public interface Stopable 
{
public void stopGame();    
}
