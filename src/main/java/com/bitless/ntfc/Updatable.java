package com.bitless.ntfc;

/**
 *
 * @author pavulzavala
 */
@FunctionalInterface
public interface Updatable 
{

     public void update( float l );
    
}
