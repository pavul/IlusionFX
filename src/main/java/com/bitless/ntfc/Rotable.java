package com.bitless.ntfc;

/**
 * used to make rotate sprites or
 * other entities
 * @author pavulzavala
 */
@FunctionalInterface
public interface Rotable 
{
    public void rotate();
}//
