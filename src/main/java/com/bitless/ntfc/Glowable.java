
package com.bitless.ntfc;

/**
 * this interface proved 2 methods to set the 
 * Glow Object settings
 * @author PavulZavala
 */
public interface Glowable 
{

    public void setGlowLevel( float newGlowLevel );
    public void restoreGlow();

}//

