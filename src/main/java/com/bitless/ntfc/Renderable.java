package com.bitless.ntfc;

import javafx.scene.canvas.GraphicsContext;

/**
 * this interface ins commonly implemented by levels
 * to render all that is being drawn on it
 * @author pavulzavala
 */
public interface Renderable 
{
    public void render( GraphicsContext preRenderer );
}
