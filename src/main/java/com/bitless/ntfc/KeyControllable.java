package com.bitless.ntfc;

import javafx.event.EventTarget;

/**
 * methods to control with keyboard
 * @author pavulzavala
 */
public interface KeyControllable 
{
    public void keyReleased( EventTarget e );
    public void keyTyped( EventTarget e );
    public void keypressed( EventTarget e );   
}//
