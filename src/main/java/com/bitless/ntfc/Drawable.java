package com.bitless.ntfc;

import javafx.scene.canvas.GraphicsContext;

/**
 * this is used for sprites to draw the imagen
 * or frames on an animation
 * @author pavulzavala
 */
@FunctionalInterface
public interface Drawable 
{
    public void draw(GraphicsContext g);
}
