package com.bitless.ntfc;

/**
 *
 * @author PavulZavala
 */
public interface GameResizable 
{
    /**
     * this functions scales normal viewWidth and viewHeight of the
     * game to full scree maintaining the aspect ratio
     */
    public void setGameResizable();
    
    public void setFullScreen();
    
}//
