package com.bitless.ntfc;

/**
 * this interface is used to implement lambdas to
 * any other generic process when necessary
 * @author PavulZavala
 */
@FunctionalInterface
public interface Executable 
{
    public void execute();   
}
