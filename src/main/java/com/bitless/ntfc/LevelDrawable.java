package com.bitless.ntfc;

import javafx.scene.canvas.GraphicsContext;

/**
 *this interface provide 3 methods used by levels
 * to draw diferent layers, background, foreground, and HUD
 * @author pavulzavala
 */
public interface LevelDrawable 
{
    
    /**
     * used to draw backgrounds inside draw method of a level
     * @param g 
     */
    public void drawBackground( GraphicsContext g );
    
    /**
     * used to draw sprites inside draw method of a level
     * @param g 
     */
    public void drawForeground( GraphicsContext g );
    
    /**
     * used to draw HUD inside draw method of a level
     * @param g 
     */
    public void drawHUD( GraphicsContext g );
    
}
