
package com.bitless.ntfc;


/**
 *
 * @author PavulZavala
 */
@FunctionalInterface
public interface Responsable 
{
    public void sendData();
    
}//
