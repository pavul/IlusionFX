/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bitless.ntfc;

/**
 * this interface provide a method to update image frames
 * so called animatios of sprites
 * @author PavulZavala
 */
@FunctionalInterface
public interface Animationable 
{
    public void updateAnimation();
}
