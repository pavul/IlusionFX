package com.bitless.ntfc;

/**
 * to init diferent parts of the level,
 * this must be implemented by level instance 
 * and they need to be called inside of the constructor of each level
 * @author pavulzavala
 */
public interface Initiable 
{
    
    /**
     * init all data, from property or external files, calls to services
     */
    public void initData();
    
    /**
     * init all sound (audioclip, mediaplayer) instances as well as load
     * all sound and music files
     */
    public void initSound();
    
    /**
     * load all images and set the animationList,
     * i suggest to animationList.clear(); before
     * init all images on each level
     */
    public void initImages();
    
    /**
     *  set all sprites of each level
     */
    public void initSprite();
    
    /**
     * load all bacground images, tiles and set all proper background instances
     */
    public void initBackground();
    
    /**
     * same as initSprite but this can be a new layer
     */
    public void initForeground();
    
    /**
     * load or init all files to be used as HUD
     */
    public void initHUD();
    
}
