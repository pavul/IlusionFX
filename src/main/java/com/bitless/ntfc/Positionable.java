package com.bitless.ntfc;

/**
 *
 * @author pavulzavala
 */
@FunctionalInterface
public interface Positionable 
{
    /**
     * this will set the entity to position X & Y
     * @param x
     * @param y 
     */
    public void setPosition( float x, float y );
    
}
