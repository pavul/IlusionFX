
package com.bitless.ntfc;

import com.bitless.graphicentity.color.PaintColor;
import javafx.scene.effect.ColorAdjust;

/**
 * this interface provides methods to set a new 
 * ColorAdjust to the sprite.
 * color adjust means change the color of the sprite, 
 * to red , white, yellow etc. ideally to represent if 
 * the sprite was hit
 * @author PavulZavala
 */
public interface ColorAdjustable 
{
    public void setColor( PaintColor colorToSet );
    
    public void setColor( ColorAdjust colorToSet );
    
    public void restoreColor();
}//
