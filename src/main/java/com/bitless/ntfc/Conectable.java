/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bitless.ntfc;

import java.io.IOException;

/**
 * used to connect clients to a server
 * @author PavulZavala
 */
public interface Conectable 
{
   public void connect();
}//
