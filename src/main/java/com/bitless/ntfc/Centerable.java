/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bitless.ntfc;

/**
 * this interface provides methods to get Center on X axis 
 * and Center on Y axis
 * @author PavulZavala
 */
public interface Centerable 
{
    public float getCenterX();
    public float getCenterY();
}//
