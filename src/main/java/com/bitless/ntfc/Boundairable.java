package com.bitless.ntfc;

/**
 * sets the boundaires of an sprite, it can be used 
 * by a collider or sprite depending the use
 * @author pavulzavala
 */
public interface Boundairable 
{
    
    /**
     * returns x of sprite
     * @return 
     */
    public float getLeftBound();
    
    /**
     * returns x + width of sprite
     * @return 
     */
    public float getRightBound();
    
    /**
     * returns y of sprite
     * @return 
     */
    public float getTopBound();
    
    /**
     * returns y + height of sprite
     * @return 
     */
    public float getBottomBound();
    
    
}//
