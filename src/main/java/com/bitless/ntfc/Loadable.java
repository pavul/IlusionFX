package com.bitless.ntfc;

/**
 * 
 * @author pavulzavala
 */
public interface Loadable 
{
   
    /**
     * method used to load or execute something
     * from gameManager when gameManager.loadLevel is called
     */
    public void onGameloaded();
    
}
