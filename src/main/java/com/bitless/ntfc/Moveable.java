package com.bitless.ntfc;

/**
 *
 * @author pavulzavala
 */
public interface Moveable 
{
   
    /**
     * move entity the specified ammount of X
     * @param x 
     */
    public void moveX( float x );
    
    /**
     * move entity the specified ammount of Y
     * @param y 
     */
    public void moveY( float y );
    
    /**
     * move entity with specified ammount of X & Y
     * @param x
     * @param y
     */
    public void moveXY( float x, float y);
    
    /**
     * move the entity with specified spdX
     */
    public void moveXSpd();
    
    /**
     * move the entity with specified spdY
     */
    public void moveYSpd();
    
    
    
    
    
}//
