package com.bitless.camera;

import com.bitless.ntfc.Moveable;

/**
 * clase que crea una camara que renderiza el room a lo largo de una ventana de puerto
 * portview
 * 
 * this class create a camera who render certain part of the level, if the level
 * is bigger than portview
 * 
 * @author pavulzavala
 */
public class Camera implements Moveable
{
    
    /**
     * NOTA: para ver que se mueve la escena a la derecha, se debe de mover la camara en X --
     * para ver que se mueva la escena a la izquierda, se debe de mover la camara en Y
     */
    
    /**
     * x axis position of cammera
     */
    float camx; //posicion x en que se encuentra la camara
    
    /**
     * y axis position of cammera
     */
    float camy; //posicion y en que se encuentra la camara
    
    /**
     * minimum value allowed for camera on X axis
     * expected value 0
     */
    float camxMin; //posicion minima que tiene la camara, por lo general 0 en eje x
    
    /**
     * maximum value allowed for camera on X axis
     * expected value roomWidth
     */
    float camxMax; //posicion maxima para mover la camara -roomWidth + viewxport
    
    /**
     * minimum value allowed for camera on Y axis
     * expected value 0
     */
    float camyMin; //posicion minima que tiene la camara, por lo general 0 en eje y
    
    /**
     * maximum value allowed for camera on Y axis
     * value expected roomHeight
     */
    float camyMax; //posicion maxima para mover la camara -roomHeight +viewyport
    
    /**
     * a room can be huge, but we aren't suppose to show everything, to show a part of the 
     * room there will be viewWidth and viewHeight
     */
    int roomWidth; //el ancho del room del nivel este puede ser de muchos pixeles de ancho
    int roomHeight; //el alto del room del nivel este puede ser de muchos pizeles de alto
    float viewWidth; //el ancho de porcion de pantalla que se vera del nivel
    float viewHeight; //el alto de porcion de pantalla que se vera del nivel
    
    /**
     * this is a value that will be in the same place always on the viewport, 
     * if some level is implementing camera and want to show HUD, that
     * hud must take viewX and viewY to show that HUD always in the same place
     * of the view
     */
    float viewX; //posicion estatica que siempre esta en el view de la ventana
    // es decir, si se pone un texto en x = viewX, siempre se vera en esa posicion
   
    /**
     * this is a value that will be in the same place always on the viewport, 
     * if some level is implementing camera and want to show HUD, that
     * hud must take viewX and viewY to show that HUD always in the same place
     * of the view
     */
    float viewY;//lo mismo que viewX, pero para el eje Y
    
    //margenes para que cuando pasa el personaje se muevan la camara de la pantalla y
    //muestre mas espacio del room, por default estan inactivos con el valor -1
    
    /**
     * left margin to check if the player is reaching that to move the camera to left
     */
    float marginLeft= -1; 
    
    /**
     * right margin to check if the player is reaching that to move the camera to right
     */
    float marginRight= -1;
    
    /**
     * top margin to check if the player is reaching that to move the camera to top
     */
    float marginTop= -1;
    
    /**
     * bottom margin to check if the player is reaching that to move the camera to bottom
     */
    float marginBottom= -1;
            
    
    /**
     * constructor 1, 
         creates a camera defining the width and heigth values of the room ( these values 
         are the boundaries of the camera ),
         and the portion that has to show in the screen ( viewWidth, viewHeight )

         create a camera object definig the size for the room and the size for 
         the view port to show in the screen
     * 
     * @param roomWidth
     * @param roomHeight
     * @param viewXPort
     * @param viewYPort
     */
    public Camera(int roomWidth, int roomHeight, int viewWidth,int viewHeight)
    {
        
        this.roomWidth = roomWidth;
        this.roomHeight = roomHeight;
        this.viewWidth = viewWidth;
        this.viewHeight = viewHeight;
        
        this.camxMin = 0;
        this.camxMax = -roomWidth + viewWidth;
        this.viewX = camx;
        this.camyMin = 0;
        this.camyMax = -roomHeight + viewHeight;
        this.viewY = camy;
        
    }//const1

    public float getCamx() {
        return camx;
    }

    public void setCamx(float camx) {
        this.camx = camx;
    }

    public float getCamy() {
        return camy;
    }

    public void setCamy(float camy) {
        this.camy = camy;
    }

    public float getCamxMin() {
        return camxMin;
    }

    public void setCamxMin(float camxMin) {
        this.camxMin = camxMin;
    }

    public float getCamxMax() {
        return camxMax;
    }

    public void setCamxMax(float camxMax) {
        this.camxMax = camxMax;
    }

    public float getCamyMin() {
        return camyMin;
    }

    public void setCamyMin(float camyMin) {
        this.camyMin = camyMin;
    }

    public float getCamyMax() {
        return camyMax;
    }

    public void setCamyMax(float camyMax) {
        this.camyMax = camyMax;
    }

    public int getRoomWidth() {
        return roomWidth;
    }

    public void setRoomWidth(int roomWidth) {
        this.roomWidth = roomWidth;
    }

    public int getRoomHeight() {
        return roomHeight;
    }

    public void setRoomHeight(int roomHeight) {
        this.roomHeight = roomHeight;
    }

    public float getViewWidth() {
        return viewWidth;
    }

    public void setViewWidth(float viewWidth) {
        this.viewWidth = viewWidth;
    }

    public float getViewHeight() {
        return viewHeight;
    }

    public void setViewHeight(float viewHeight) {
        this.viewHeight = viewHeight;
    }

    public float getViewX() {
        return viewX;
    }

    public void setViewX(float viewX) {
        this.viewX = viewX;
    }

    public float getViewY() {
        return viewY;
    }

    public void setViewY(float viewY) {
        this.viewY = viewY;
    }


    


    public float getMarginLeft() {
        return marginLeft;
    }

    /**
     * this method specifies the left boundary that the camera
     * has to check when a sprite si approaching to the left edge
     * @param marginLeft 
     */
    public void setMarginLeft(float marginLeft) {
        this.marginLeft = marginLeft;
    }

    
    
    public float getMarginRight() {
        return marginRight;
    }

    /**
     * * this method specifies the right boundary that the camera
     * has to check when a sprite si approaching to the right edge
     * @param marginRight
     */
    public void setMarginRight( float marginRight ) {
        this.marginRight = marginRight;
    }

    public float getMarginTop() {
        return marginTop;
    }

    /**
     * * this method specifies the top boundary that the camera
     * has to check when a sprite is approaching to the top edge
     * @param marginTop
     */
    public void setMarginTop( float marginTop ) {
        this.marginTop = marginTop;
    }

    public float getMarginBottom() {
        return marginBottom;
    }

    /**
     * * this method specifies the bottom boundary that the camera
     * has to check when a sprite is approaching to the bottom edge
     * @param marginBottom
     */
    public void setMarginBottom( float marginBottom ) {
        this.marginBottom = marginBottom;
    }
    
    /**
     * moves camera on X axis, 
     * if x value is positive camera will be moved to right,
     * if x value is negative camera will be moved to left
     * @param x speed to move camera on x axis
     */
    @Override
    public void moveX( float x )
    {
//      to move camera to right we have to substract x to camx(in negative way),
//      at the same time x it should be added to ViewX to have the correct position
//      in order to display some text in the same position of the view 
        
          this.camx -= x;
          this.viewX += x;
         
        /**
         * minimum value of camx should be 0, is the starting point
         * camx is not mayor than 0
         */
        if( this.camx >= camxMin )
        {
            this.camx = camxMin;
            this.viewX = camxMin;
        }
        /**
         * camx also is max position is -level Width + viewWidth, if we
         * move more than that we can leave the bounds of the level, hence
         * draw can display some inconsistencies on screen
         */
        else if( this.camx <= camxMax )
        {
            //establece que se muestre el room a  -roomwidth + viewXwidth
            this.camx = camxMax;
            this.viewX = camxMax * -1;
        }
        
        
    }//
    
    
    //@TODO hacer el bound para los margenes de la posicion Y
    /**
     * moves camera on Y axis, 
     * if y value is positive camera will be moved down,
     * if y value is negative camera will be moved up
     * @param y speed to move camera on y axis
     */
    @Override
    public void moveY( float y )
    {
        this.camy -= y; 
        this.viewY += y;
        
        //establece que se muestre el room a cero
        if(this.camy >= camyMin)
        {
            this.camy = camyMin;
            this.viewY = camyMin;
        }
        else if(this.camy <= camyMax)
        {
            //establece que se muestre el room a  -roomHeight + viewXheigth
            this.camy = camyMax;
            this.viewY = camyMax * -1;
        }
        
    }//moveY

    
    
     /**
     * metodo que mueve la camara sobre el eje X e Y
     * 
     * moves camera among X and Y axis with the speed defined 
     * by x and y, internally this method is calling 
     *  moveX(x);
     *  moveY(y);
     * @param x 
     * @param y 
     */
    @Override
    public void moveXY(float x, float y) {
        moveX(x);
        moveY(y);
}//

   

    
/**
 * do not use this method on camera
 */
    @Override
    public void moveXSpd() {
    }

    
/**
 * do not use this method on camera
 */
    @Override
    public void moveYSpd() {
    }
    
    
    
    
    
     
    /**
     * this is only for testing purposes, must not be ued on
     * production
     * @return 
     */
    @Override
    public String toString() 
    {
        return "Camera{" + "camx=" + camx + ", camy=" + camy + ", camxMin=" + camxMin + ", camxMax=" + camxMax + ", camyMin=" + camyMin + ", camyMax=" + camyMax + ", roomWidth=" + roomWidth + ", roomHeight=" + roomHeight + ", viewXPort=" + viewWidth + ", viewYPort=" + viewHeight + ", viewX=" + viewX + ", viewY=" + viewY + ", marginLeft=" + marginLeft + ", marginRight=" + marginRight + ", marginTop=" + marginTop + ", marginBottom=" + marginBottom + '}';
    }
    
    
}//class
