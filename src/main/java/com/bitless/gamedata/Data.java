
package com.bitless.gamedata;

/**
 * this will contain the data to use along the game
 * that can be stored/loaded
 * @author PavulZavala
 */
public class Data 
{
    private String name;
    private String value;
    private boolean persistent;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public boolean isPersistent() {
        return persistent;
    }

    public void setPersistent(boolean persistent) {
        this.persistent = persistent;
    }


    
}
