package com.bitless.gamedata;

import com.bitless.cfg.Config;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Set;

/**
 * this class contains methods to set/get data used by levels, menus, etc
 * even contains methods to load that data from properties file,
 * property file must be located in []
 * @author PavulZavala
 */
public class GameData //implements Datable 
{
    private static final List< Data > DATALIST = new ArrayList<>();

    private static String fileName = "gameData.properties";
    
    
    /**
     * this function will set the new value to a property with dataName;
     * if dataName exists value will be updated
     * if not it will be created and then added,
     * if persist is equals true means that value will be persisted in 
     * the .properties file when you decide to save this gameData
     * @param dataName
     * @param dataValue
     * @param persistData 
     */
//    @Override
    public static void setData(String dataName, String dataValue, boolean persistData ) 
    {
        Data dataField = DATALIST.stream().filter( dataProperty ->
                {
                  return dataProperty.getName().equals( dataName );
                } )
                .findFirst()
                .orElse( null );
        
        
        if( null != dataField )
        {
            dataField.setValue( dataValue );
        }
        else
        {
            dataField = new Data();
            dataField.setName( dataName );
            dataField.setValue( dataValue );
            
            DATALIST.add( dataField );
        }
        
        dataField.setPersistent( persistData );
    }

    /**
     * this will return the value of the specified "dataName",
     * if value is not found then "0" will return
     * @param dataName
     * @return 
     */
    public static String getData(String dataName) 
    {
        Data dataField = DATALIST.stream().filter( dataProperty ->
        {
          return dataProperty.getName().equals( dataName );
        } )
        .findFirst()
        .orElse( null );

        if( null != dataField )
        {
            return dataField.getValue(); 
        }
          
        return "0";
    }

    /**
     * this will save all data stored in GAMEDATA list only
     * whether persistent value is true, all others will be
     * ignored
     * @throws FileNotFoundException
     * @throws IOException 
     */
//    @Override
    public static void saveData() throws FileNotFoundException, IOException
    {
        
        if( DATALIST.isEmpty() )
        { return; }
        
        Properties gameProperties =  new Properties();
     
        OutputStream output = new FileOutputStream( Config.BASEPATH + fileName );
        
        DATALIST.forEach( data ->
        {
            if( data.isPersistent() )
            {
                gameProperties.setProperty( data.getName() , data.getValue() );
            }
        });
        
        gameProperties.store( output, null );
        
    }

    /**
     * this functions load all data that is stored in 
     * gameData.properties file and returns the proper set so it
     * can be used in the game
     * @throws FileNotFoundException
     * @throws IOException 
     */
//    @Override
    public static void loadData() throws FileNotFoundException, IOException
    {
        Properties gameProperties =  new Properties();
       
        gameProperties.load( new FileInputStream( Config.BASEPATH + fileName ) );
        
        Set<String> propertySet = gameProperties. stringPropertyNames();
        
        propertySet.forEach(propertyName ->
        {
            
            Data fieldData = new Data();
            
            fieldData.setName( propertyName );
            
            fieldData.setValue( gameProperties.getProperty( propertyName ) );
            
            fieldData.setPersistent( true );
            
            DATALIST.add( fieldData );
            
        });
   
    }

    /**
     * this method will override the name of the properties
     * files to be used to load/save data, fileName by default
     * is: gameData.properties
     * @param fileName 
     */
    public static void setFileName(String fileName) {
        GameData.fileName = fileName;
    } 
    
    /**
     * used to clear the list of data that holds all
     * the data, this does not overrides all data values
     */
    public static void clearData()
    {
    DATALIST.clear();
    }
    
    public static List<Data> getDataList()
    {
    return DATALIST;
    }
    
}//