
package com.bitless.gamedata;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

/**
 *
 * @author PavulZavala
 */
public interface Datable 
{
   
    public void setData( String dataName, String dataValue, boolean persistData );
    
    public String getData( String dataName );
     
    public void saveData() throws FileNotFoundException, IOException;
    
    public List<Data> loadData() throws FileNotFoundException, IOException;
    
    
}//
