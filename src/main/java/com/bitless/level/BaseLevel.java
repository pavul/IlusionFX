package com.bitless.level;

import com.bitless.audio.AudioManager;
import com.bitless.camera.Camera;
import com.bitless.cfg.GameState;
import com.bitless.graphicentity.BaseSprite;
import com.bitless.manager.GameManager;
import com.bitless.ntfc.Updatable;
import com.bitless.ntfc.Renderable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javafx.event.EventHandler;
import javafx.scene.image.Image;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;

/**
 * this is the base level of a game, it can be extended
 * to be able to complete each  diferent level
 * @author pavulzavala
 */
public abstract class BaseLevel 
              implements Renderable, Updatable
{
   
    protected int levelWidth;
    protected int levelHeight;
   
    
    /**
     * all sprites created will be inside this list, 
     * when a sprite is created in the level should be added
     * to this list, every level will have it own list
     */
    protected final List<BaseSprite> spriteList = new ArrayList<>();
    
        
    /**
     * this will save all image frames that can be used
     * by the sprites, HUD or other graphic entities in a level, 
     * this object must be load all images with the corresponding String ID
     * on Initiable.initImages() method.
     * if a sprite in the game needs to change the animation then, those
     * frames can be retrieved from this list
     */
    protected final Map<String, Image[]> imagesList = new HashMap<>();
    
    
    //specify which is the actual state of the game
    //if loading, playing, gameover, etc...
    protected GameState gameState;
    
    //direct access to Canvas NODE where all the
    //game is beign displayed
    public GameManager gm;
    
    /**
     * cammera object to traverse along big levels
     * this contains viewWidth and viewHeight
     */
    protected Camera camera;
    
    /**
     * below variables are to implement keyboard input listeners
     * they are handled by GameManager (canvas) but the implementation
     * can be done in each level, or in a separate interface for easy ussage
     * <b>
     * WHEN LEVEL IS SCALED YOU NEED TO BE CAREFUL ABOUT MOUSE X & Y POSITION
     * IF YOU LEVEL IS 640x480 AND IS SCALED, X & Y POSITION WILL BE MORE
     * THAN 640x480, THEY WILL BE SCREEN SIZE EXPANDED
     * </b>
     */
    public EventHandler<? super KeyEvent> keyReleasedControl;
    public EventHandler<? super KeyEvent> keyTypedControl;
    public EventHandler<? super KeyEvent> keyPressedControl;
    public EventHandler<? super MouseEvent> mouseClickedControl;
    public EventHandler<? super MouseEvent> mousePressedControl;
    public EventHandler<? super MouseEvent> mouseReleasedControl;
    public EventHandler<? super MouseEvent> mouseMovedControl;
    
    
    
    //sprites, tiles/brackgrounds
    
    /**
     * this instance contains methods to play mp3 music background
     * and .wav sound effects
     */
    protected AudioManager audioManager;
    
    
    /**
     * this constructor only set width and height of the level, for
     * games where there is no cammera and the whole game is inside
     * this level
     * @param levelWidth
     * @param levelHeight
     */
    public BaseLevel( int levelWidth, int levelHeight )
    {
    
        gameState = GameState.LOADING;
        this.levelWidth = levelWidth;
        this.levelHeight =  levelHeight;
        
        /**
         * instance to manage bg music and sounds created
         */
        audioManager = new AudioManager();
       
    }//
    
    
    /**
     * this constructor set width height, viewWidth, viewHeigth, and creates 
     * the camera with those level measures, 
     * <b>
     * NOTE: level width and height should be bigger than view width and height,
     * if viewWidth and ViewHeight are same as level width and height, methods
     * to move camera wont work
     * </b>
     * @param levelWidth
     * @param levelHeight
     * @param viewWidth
     * @param viewHeight
     */
    public BaseLevel( int levelWidth, int levelHeight, int viewWidth, int viewHeight)
    {
        
        this( levelWidth, levelHeight );
        
        //set cammera properties
        this.camera = new Camera(levelWidth, levelHeight, viewWidth, viewHeight);
     
    }//const2
    
    
//GETTERS AND SETTERS

    
    /**
     * used when GameManager is loading a new level, so
     * we can be able to call gm.loadLevel again from
     * other levels
     * @param gm 
     */
    public void setGm(GameManager gm) {
        this.gm = gm;
    }

    public GameManager getGm() {
        return gm;
    }

    public int getLevelWidth() {
        return levelWidth;
    }

    public void setLevelWidth(int levelWidth) {
        this.levelWidth = levelWidth;
    }

    public int getLevelHeight() {
        return levelHeight;
    }

    public void setLevelHeight(int levelHeight) {
        this.levelHeight = levelHeight;
    }

    public Camera getCamera() {
        return camera;
    }

    
    
}//
