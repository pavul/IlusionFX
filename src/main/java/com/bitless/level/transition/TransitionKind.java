/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bitless.level.transition;

/**
 *
 * @author PavulZavala
 */
public enum TransitionKind 
{
    RECT_LEFT_TO_RIGHT,
    RECT_RIGHT_TO_LEFT,
    RECT_TOP_TO_BOTTOM,
    RECT_BOTTOM_TO_TOP   
}//
