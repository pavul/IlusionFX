package com.bitless.level.transition;

/**
 *
 * @author PavulZavala
 */
public enum TransitionState 
{
    CLOSED,
    OPENING,
    OPEN,
    CLOSING
}
