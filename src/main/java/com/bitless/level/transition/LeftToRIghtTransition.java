package com.bitless.level.transition;

import com.bitless.ntfc.Drawable;
import javafx.scene.canvas.GraphicsContext;

/**
 *
 * @author PavulZavala
 */
public class LeftToRIghtTransition extends BaseTransition
                                           implements Drawable
{

    @Override
    public boolean openingTransition() 
    {
        currentWidth += xVariation;
        
        return currentWidth >= maxWidth;
    }//

    @Override
    public boolean closingTransition() 
    {
        currentWidth -= xVariation;
         
        return currentWidth <= 0;
    }//

    @Override
    public void draw(GraphicsContext g) 
    {
        
        if( active )
        {
            g.setFill( color );
            if( null != camera )
            {
            
            g.fillRect( camera.getViewX(),
                        camera.getViewY(),
                        currentWidth,
                        currentHeight );
            }
            else
            {
            g.fillRect(0, 0, currentWidth, currentHeight);
            }
            
        
        }
        
    }

    
}//
