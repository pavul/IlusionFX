package com.bitless.level.transition;

import com.bitless.ntfc.Executable;

/**
 * @author PavulZavala
 */
public interface Transitionable 
{
   /**
    * process to execute when a transition is opening
     * @return true when this process is done
    */
    public boolean openingTransition();
    
    /**
     * process to execute when a transition is closing
     * @return true when this process is done
    */
    public boolean closingTransition();
    
    /**
     * this method will execute lambda passed when transition is open 
     * @param executable lambda to execute when state changes to OPEN
     */
    public void toExecuteWhenOpen( Executable executable );
    
    
}//
