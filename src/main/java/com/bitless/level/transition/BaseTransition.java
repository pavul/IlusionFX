package com.bitless.level.transition;

import com.bitless.camera.Camera;
import com.bitless.ntfc.Drawable;
import com.bitless.ntfc.Executable;
import javafx.scene.paint.Color;

/**
 *
 * @author PavulZavala
 */
public abstract class BaseTransition 
                      implements Transitionable
{
 
    protected float alpha;
//    protected TransitionKind kind;
    protected TransitionState state;
 
    //if transition is active, it will be processed each step
    protected boolean active; 
    
    protected int maxWidth;
    protected int maxHeight;
    protected int currentWidth;
    protected int currentHeight;
    
    
    protected int xVariation;
    protected int yVariation;
    
    protected Executable executable;
    
    protected Color color;
    
    protected Camera camera;
    
    /**
     * Level transitions must be set with setters
     */
    public BaseTransition()
    {
        alpha = 1;
        state = TransitionState.CLOSED;
        active = true;
        maxWidth = 0;
        maxHeight = 0;
        xVariation = 1;
        yVariation = 1;
        color = Color.BLACK;
        currentWidth = 0;
        currentHeight = 0;
    }//
    
    
    public void processTransition()
    {
    if( !active ){ return; }
    
        switch( state )
        {
            /**
             * when transition is done, state changes to closed 
             * and active is set to false, so ite cannot be 
             * processed for the game until is activated
             */
            //            case CLOSED:
            //                state = TransitionState.CLOSED;
            //                active = false;
            //                break;

            case OPENING:
                if( openingTransition() )
                { state = TransitionState.OPEN; }
                break;

            case OPEN:
                    //executeOpenTransition();
                    if( null != executable )
                    {
                    executable.execute();
                    }
                break;

            case CLOSING:
                if( closingTransition() )
                {
                state = TransitionState.CLOSED;
                active = false;
                }
                break;
        }//
     
    }//

       
    @Override
    public void toExecuteWhenOpen( Executable executable )
    {
    this.executable = executable;
    }

    
    //GETTERS & SETTERS
    
    public void setxVariation(int xVariation) {
        this.xVariation = xVariation;
    }

    public void setyVariation(int yVariation) {
        this.yVariation = yVariation;
    }

    public float getAlpha() {
        return alpha;
    }

    public void setAlpha(float alpha) {
        this.alpha = alpha;
    }

    public TransitionState getState() {
        return state;
    }

    public void setState(TransitionState state) {
        this.state = state;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public int getMaxWidth() {
        return maxWidth;
    }

    public void setMaxWidth(int maxWidth) {
        this.maxWidth = maxWidth;
    }

    public int getMaxHeight() {
        return maxHeight;
    }

    public void setMaxHeight(int maxHeight) {
        this.maxHeight = maxHeight;
    }

    public int getCurrentWidth() {
        return currentWidth;
    }

    public void setCurrentWidth(int currentWidth) {
        this.currentWidth = currentWidth;
    }

    public int getCurrentHeight() {
        return currentHeight;
    }

    public void setCurrentHeight(int currentHeight) {
        this.currentHeight = currentHeight;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public void setCamera(Camera camera) {
        this.camera = camera;
    }

    

    
    /**
     * this method will be executed while Transition
     * is in opening state, usually to increase
     * width and height to open the transition
     */
//    protected abstract void openingTransition();
//    {
//        if( currentWidth < maxWidth )
//        {
//            currentWidth += increment;
//        }
//    
//    }
    
    /**
     * this method will be executed while Transition is
     * in closing state, usually to decrease width and 
     * height
     */
//    protected abstract void closingTransition();
//    {
//        if( currentWidth > 0 )
//        {
//        currentWidth -= increment;
//        }
//    }
    
    
    
}//
