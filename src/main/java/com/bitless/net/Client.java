
package com.bitless.net;

import com.bitless.cfg.Config;
import com.bitless.ntfc.Conectable;
import com.bitless.ntfc.Requestable;
import com.bitless.ntfc.Responsable;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author PavulZavala
 */
public class Client 
        implements Conectable
{

    protected DatagramSocket socket;
    protected String ip;
    protected int port;
    protected int id;
    private Thread connectThread;
//    protected boolean isConnected;
    
    
    /**
     * constructor without arguments to use with getters and setters
     * @throws java.net.SocketException
     */
    public Client() throws SocketException
    {
        this.socket = new DatagramSocket();
        id = 0;
        //id set after increasement
        //id = ++Config.SOCKET_ID_COUNTER; 
    }
    
    /**
     * this constructor creates a client indicating the ip and port
     * of the server
     * @param ip
     * @param port
     * @throws SocketException 
     */
    public Client( String ip, int port ) throws SocketException
    {
    this();
    this.setIp( ip );
    this.setPort(port);
    }

    public DatagramSocket getSocket() {
        return socket;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    

    /**
     * this method send a request to the server to connect
     */
    @Override
    public void connect()
    {
        try 
        {
            //send connect request to server
            send( "connect" );
        }
        catch (IOException ex) 
        {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        }
       
        
        connectThread = 
            new Thread( ()->
        {
            while( id == 0 )
            {
                 DatagramPacket response =
                            new DatagramPacket(new byte[ Config.UDP_BUFFER_SIZE], Config.UDP_BUFFER_SIZE );
                    
                try 
                {
                   
                    socket.receive( response );
                    
                    String resp = new String( response.getData(), "UTF-8" );
                    resp = resp.trim();
                    System.err.println("getting DATA: "+resp);
                    
                    
                    if( resp.trim().contains( "connected" ) )
                    {
//                        String[] txt = resp.split( "#" );
//                        System.err.println( "TXT size: "+txt.length );
//                        for( String s: txt )
//                        {
//                        System.err.println( s );
//                        }
                        
                        
                    id = Integer.parseInt( resp.trim().split( "#" )[0] ) ;
                    socket.connect( InetAddress.getByName( ip ), port );
                    stopConnecting();
                    }
                    
                    
                } 
                catch ( IOException | InterruptedException ex) 
                {
                    Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
                }
                
            }
        
        });
        
        connectThread.start();
        
    }

    /**
     * method used to receive responses from server,
     * every time this method is called a new Thread is created
     * be careful not to call many times this
     * @param r 
     */
    public void receive( Requestable r )
    {
        new Thread(()->
        {
            while( true )
            {
            r.receiveData();
            }
        
        }).start();
    
    }
    
    /**
     * this method is used to send requests to server using a lambda
     * you have to create databuffer to send, datagramPacketRequest
     * specify ip and port.
     * @param r 
     */
    public void send( Responsable r )
    {
    r.sendData();
    }
    
    
    /**
     * this method will send a request to the server to
     * the specific IP and port set by this class
     * @param data request data
     * @throws UnknownHostException 
     */
    public void send( String data ) throws UnknownHostException, IOException
    {
        byte[] dataBuf = data.getBytes();
        DatagramPacket request = 
                new DatagramPacket(dataBuf, 
                        dataBuf.length, InetAddress.getByName( ip ), port );
    
        socket.send( request );
    }
    
    /**
     * this method kills Thread used that is created
     * when we attempt to connect to the server
     * @throws InterruptedException 
     */
    public void stopConnecting() throws InterruptedException
    {
    connectThread.join();
    }
}//
