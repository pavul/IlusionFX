
package com.bitless.net;

import com.bitless.cfg.Config;
import com.bitless.ntfc.Conectable;
import com.bitless.ntfc.Requestable;
import com.bitless.ntfc.Responsable;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**@TODO consider add variable to specify number of clients
 * this class contains main server connection with all clients
 * connected to a game, this connection is using UDP and it is really
 * simple, if you need to use other kind of connection you are free
 * to create your own
 * @author PavulZavala
 */
public class Server 
        implements Conectable
{

    protected DatagramSocket serverSocket;
    
    protected boolean isAccepting;
    
    protected List<Client> clientList;
    
    protected String ip;
    
    protected int port;

    private Thread connectThread;
    
    
    /**
     * 
     * @param port
     * @throws IOException 
     */
    public Server( int port ) throws IOException
    {
        serverSocket = new DatagramSocket( port );
        
        this.port = port;
        this.isAccepting = true;
        clientList = new ArrayList<>();
    }//
    
    
    /**
     * Accept UDP connections and store in clientList
     * ----------------------------------------------
     * this method is used to receive packages from UDP clients, 
     * and store their IP and ADDRESS in the client list, 
     * - you can change isAccepting to false to no receive more
     * client connections or simple call stopIsAcception to finish
     * the Tread.
     * @TODO it can be changed to accept like server socket
     */
    @Override
    public void connect()
    {
       connectThread = new Thread( ()->
        {
            
            while( isAccepting )
            {
                try {
                    //datagram packet to receive incomming request from client
                    DatagramPacket request = 
                            new DatagramPacket( new byte[ Config.UDP_BUFFER_SIZE ], Config.UDP_BUFFER_SIZE );
                    
                    serverSocket.receive( request );
                    
                    //get Port and Address from client
                    //and check if exists in clientList
                    Client c = clientList
                            .stream()
                            .filter( client ->
                            {
                                return client.getIp().equals( request.getAddress().getHostAddress() );
                            }).findFirst().orElse( null );
                    
                    //if no exists add it and send response
                    if( null == c )
                    {
                        Client client = new Client();
                        client.setIp( request.getAddress().getHostAddress() );
                        client.setPort( request.getPort() );
                        client.setId( generateId() );
                        
                        //adding new client to the list
                        clientList.add( client );
                        
                        byte[] bufResp = (client.getId() + "#connected").getBytes( "UTF-8" );
                       DatagramPacket resp = 
                               new DatagramPacket(bufResp, bufResp.length, 
                                       InetAddress.getByName( client.getIp() ), 
                                       client.getPort());
                       
                       System.err.println( client.getId()+ " Connected, response Sent" );
                       serverSocket.send( resp );
                      
                    }//
                    
                } //
                catch (IOException ex) 
                {
                    Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
                }
                
            }
        });//.start();
        connectThread.start();
    }//
    
    
    /**
     * this stops thread to accepts client socket connections
     * @throws java.lang.InterruptedException
     */
    public void stopAccepting() throws InterruptedException
    {
        connectThread.join();
    }
    
    /**
     * this closes the DatagramSocket that is acting
     * as server
     */
    public void closeServer()
    {
        serverSocket.close();
    }
    
    /**
     * used to receive UDP packets from clients
     * this method creates its own Thread so it can
     * receive packages without blocking the game
     * @param r
     */
    public void receive( Requestable r)
    {
        new Thread(()->
        {
            while( true )
            {
            r.receiveData();
            }
        }).start();   
    }//
    
    /**
     * used to generate id for connected clients
     * @return 
     */
    private int generateId()
    {
        return ++Config.SOCKET_ID_COUNTER;
    }
    
    /**
     * used to send UDP packets to clients
     * @param r
     */
    public void send( Responsable r )
    {
        r.sendData();
    }

    public DatagramSocket getServerSocket() {
        return serverSocket;
    }

    public void setServerSocket(DatagramSocket serverSocket) {
        this.serverSocket = serverSocket;
    }

    public boolean isIsAccepting() {
        return isAccepting;
    }

    public void setIsAccepting(boolean isAccepting) {
        this.isAccepting = isAccepting;
    }

    public List<Client> getClientList() {
        return clientList;
    }

    public void setClientList(List<Client> clientList) {
        this.clientList = clientList;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }
    
    
    
    
    
    
}//
