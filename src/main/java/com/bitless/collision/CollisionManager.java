/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bitless.collision;

/**
 * this class contains many methods to check
 * collisions between colliders of sprites, tiles, backgrounds etc
 * 
 * @author pavulzavala
 * @deprecated this is now deprecated, will change to CollisionUtil after all methods are improved and tested
 */
public class CollisionManager 
{
//    public static CollisionManager instance = null;
// 
//    
//    public static  String COLISION_TOP = "TOP";
//    public static  String COLISION_BOTTOM = "BOTTOM";
//    public static  String COLISION_LEFT = "LEFT";
//    public static  String COLISION_RIGHT = "RIGHT";
//    public static  String COLISION_NONE = "NONE";
//    
//    
//    private CollisionManager()
//    {}
//   
//    /**
//     * return current instance of CollisionManager
//     * @return 
//     */
//    public static CollisionManager getInstance()
//    {
//        if( null == instance )
//            return new CollisionManager();
//        else
//            return instance;
//    }//
//    
//    
//    /**
//     * return distance betwen 2 points
//     * @param coor1
//     * @param val1
//     * @param coor2
//     * @param val2
//     * @return ( coor1 + val1 / 2 ) - (coor2 + val2 / 2 )
//     */
//    public float getDistance( float coor1, float val1, float coor2, float val2 )
//    {
//    float v = ( coor1 + val1 / 2 ) - (coor2 + val2 / 2 );
//    return Math.abs( v );
//    }//
//    
//    
//    /**
//    * this function is used to combine the half parts from
//    * 2 rects, circles or other shapes as well 2 sprites
//    * @param half1
//    * @param half2
//    * this will process something like:
//    * float combinedHalfHeight = h / 2 + h2 / 2; 
//    * @return 
//    */
//   public float getCombineHalf( float half1, float half2 )
//   {
//   return ( half1 / 2 ) + ( half2 / 2 );
//   }
//           
//           
//           
//   
//   /**
//    * check whether the point defined by X & Y is inside
//    * X2, Y2, W2, H2
//    * @param x
//    * @param y
//    * @param x2
//    * @param y2
//    * @param w2
//    * @param h2
//    * @return 
//    */
//   public boolean pointCollision( float x, float y, 
//           float x2, float y2, float w2, float h2 )
//   {
//       return (x >= x2  &&
//               x <= x2 + w2 &&
//               y >= y2 &&
//               y <= y2 + h2);
//   }
//   
//   /**
//    * check whether the point specified by X e Y is inside
//    * of Sprite s
//    * @param x
//    * @param y
//    * @param s
//    * @return true if point is inside sprite
//    */
//   public boolean pointCollision( float x, float y, Sprite s )
//   {
//       float x2 = s.getX() + s.getCollider().getCx();
//       float y2 = s.getY( ) + s.getCollider().getCy();
//       float w2 = s.getCollider().getCw();
//       float h2 = s.getCollider().getCy();
//       
//       return pointCollision(  x, y, x2, y2, w2, h2 );
//   }
//   
//        
//        /**
//         * checks collision between 2 circles
//         * @param cenx center X of circle 1
//         * @param ceny center Y of circle 1
//         * @param rad radius of circle 1 ( collider.width /2 )
//         * @param cenx2 center X of circle 2
//         * @param ceny2 center Y of circle 2
//         * @param rad2 radius of circle 2 ( collider.width /2 )
//         * @return 
//         * @usage circleCollision( s.getX + s.getCollider.centerX, s.getCollider.getHalfWidth,
//         * s2.getX + s2.getCollider.centerX, s2.getCollider.getHalfWidth )
//         */
//        public  boolean circleCollision( float cenx,float ceny,float rad,
//                float cenx2,float ceny2,float rad2 )
//        {
//            //get circle vectors
//            float vx = cenx - cenx2;
//            float vy = ceny - ceny2;
//            
//             //calculate distance between circles
//            float magnitude = (float)Math.sqrt( vx * vx + vy * vy );
//            
//            //get total radio of two circles
//            float totalRadio = rad + rad2;
//         
//            return magnitude < totalRadio;
//            
//        }//
//        
//        
//        /**
//         * checks collision between Sprite and circles
//         * @param s sprite to check collision 
//         * @param cenx center X of circle 
//         * @param ceny center Y of circle 
//         * @param rad radius of circle 1 ( collider.width /2 )
//            * @param fixOverlap if true will prevent two circle colliders from overlap
//         * @return 
//         * @usage circleCollision( s.getX + s.getCollider.centerX, s.getCollider.getHalfWidth,
//         * s2.getX + s2.getCollider.centerX, s2.getCollider.getHalfWidth )
//         */
//        public  boolean circleCollision( Sprite s ,float cenx,float ceny,float rad, boolean fixOverlap )
//        {
//                
//             //get circle vectors
//            float vx =  ( s.getX() + s.getCollider().getHalfWidth() ) - cenx;
//            float vy =  ( s.getY() + s.getCollider().getHalfHeight() ) - ceny;
//            
//             //calculate distance between circles
//            float magnitude = (float)Math.sqrt( (vx * vx) + (vy * vy) );
//            
//            //get total radio of two circles
//            float totalRadio = s.getCollider().getHalfWidth() + rad;
//         
//            boolean res = magnitude < totalRadio;
//            /**
//             * this will prevent two circles from
//             * overlapping
//             */
//            if( fixOverlap && res )
//            {
//                float overlap = totalRadio - magnitude;
//                float dx = vx / magnitude;
//                float dy = vy / magnitude;
//                
//                s.setX( s.getX() + (overlap * dx)  );
//                s.setY( s.getY() + (overlap * dy)  );
//            }
//            
//            return res;
//                
//        }//
//        
//        
//        /**
//         * checks collision between two sprites
//         * @param s sprite to check collision 
//         * @param s2 sprite to check collision 
//         * @param fixOverlap 
//         * @return 
//         * @usage circleCollision( s.getX + s.getCollider.centerX, s.getCollider.getHalfWidth,
//         * s2.getX + s2.getCollider.centerX, s2.getCollider.getHalfWidth )
//         */
//          public  boolean circleCollision( Sprite s ,Sprite s2, boolean fixOverlap )
//        {
//            return circleCollision( 
//                    s,
//                    s2.getX() + s2.getCollider().getHalfWidth(),
//                    s2.getY() + s2.getCollider().getHalfHeight(), 
//                    s2.getCollider().getHalfWidth(),
//                    fixOverlap );
//        }//
//        
//        
//    
//        /**
//         * checks if there is rectangle collision  between 2 Sprites
//         * @param s1
//         * @param s2
//	 * @return true if there is a collision and false if not
//	 * */
//	public  boolean rectangleColision(Sprite s1, Sprite s2)
//	{
//            return rectangleColision( 
//                    s1.getX() + s1.getCollider().getCx(),
//                    s1.getY() + s1.getCollider().getCy(),
//                    s1.getCollider().getCw(),
//                    s1.getCollider().getCh(),
//                    s2.getX() + s2.getCollider().getCx(),
//                    s2.getY() + s2.getCollider().getCy(),
//                    s2.getCollider().getCw(),
//                    s2.getCollider().getCh()      );	
//	}//colision rectangular
//        
//    
//        /**
//         * checks if there is rectangle collision  between 2 rectangles
//         * @param x
//         * @param y
//         * @param w
//         * @param h
//         * @param x2
//         * @param y2
//         * @param w2
//         * @param h2
//         * @return 
//         */
//        public  boolean rectangleColision(
//                float x, float y, float w, float h,
//                float x2, float y2, float w2, float h2)
//	{            
//
//            float combinedHalfWidth = w / 2 + w2 / 2;
//            float combinedHalfHeight = h / 2 + h2 / 2;
//
//            return ( Math.abs( getDistance( x, w, x2, w2 ) ) < combinedHalfWidth && 
//                     Math.abs( getDistance( y, h, y2, h2 ) ) < combinedHalfHeight  );
//          		
//	}//colision rectangular
//        
//    
//    
//    
//        /**
//         * checks wether there is a collision between two sprites
//         *
//         * Example:  blockRectangle(spritePlayer, box, true);
//         * @param s1
//         * @param s2
//         * @param push it push is true then sprite1 will push sprite 2
//	 * @return true if there is a colision and false if not
//	 * 
//	 * */
//	public String rectangleColision( Sprite s1, Sprite s2,boolean push )
//	{
//		String side = COLISION_NONE;
//                
//                float vx = (s1.getX()+s1.getCollider().getHalfWidth()) - (s2.getX() + s2.getCollider().getHalfWidth());
//                float vy = (s1.getY()+s1.getCollider().getHalfHeight()) - (s2.getY() + s2.getCollider().getHalfHeight());
//	
//                float combinedHalfWidth = 
//                        getCombineHalf( s1.getCollider().getCw(), s2.getCollider().getCw() );
//		
//                float combinedHalfHeight = 
//                        getCombineHalf( s1.getCollider().getCh(), s2.getCollider().getCh() );
//		
//		if( Math.abs(vx) < combinedHalfWidth )
//		{
//                    
//			if( Math.abs(vy) < combinedHalfHeight )
//			{
//				float overlapX = combinedHalfWidth - Math.abs(vx);
//				float overlapY = combinedHalfHeight - Math.abs(vy);
//				
//				if(overlapX >= overlapY)
//				{
//					if(vy>0)
//					{
//						side = COLISION_TOP;
//						if( push )s2.setY( s2.getY()-1 );
//						s1.setY(s1.getY() + overlapY);
//                                         }
//					else
//					{
//						side = COLISION_BOTTOM;
//						if( push )s2.setY(s2.getY()+1);
//                                                s1.setY(s1.getY() - overlapY);
//					}
//				
//				}
//				else
//				{
//					
//					if( vx > 0 )
//					{
//						side = COLISION_LEFT;
//						if( push )s2.setX(s2.getX()-1);
//						s1.setX( s1.getX() + overlapX );
//					}
//					else
//					{
//						side = COLISION_RIGHT;
//						if( push )s2.setX(s2.getX()+1);
//						s1.setX(s1.getX()-overlapX);
//					}
//				
//                                }//
//		
//                        }//height
//			
//		}//width
//		
//		return side;
//		
//	}//
//        
//        
//        /**
//         * rectangle collision between Sprite, and a box x, y, w, h, it can be any 
//         * rectangle or any other box, a collider can be transform to a box
//         * @param s1
//         * @param x
//         * @param y
//         * @param w
//         * @param h
//         * @return 
//         */
//        public String rectangleColision( Sprite s1, float x, float y, float w, float h )
//	{
//		String side = COLISION_NONE;
//                
//                float vx = (s1.getX()+s1.getCollider().getHalfWidth()) - ( x + (w/2) );
//                float vy = (s1.getY()+s1.getCollider().getHalfHeight()) - ( y + (h/2) );
//
//                
//                float combinedHalfWidth = 
//                        getCombineHalf( s1.getCollider().getCw(), w );
//		
//                float combinedHalfHeight = 
//                        getCombineHalf( s1.getCollider().getCh(), h );
//		
//		if( Math.abs(vx) < combinedHalfWidth )
//		{
//                    
//			if( Math.abs(vy) < combinedHalfHeight )
//			{
//				float overlapX = combinedHalfWidth - Math.abs(vx);
//				float overlapY = combinedHalfHeight - Math.abs(vy);
//				
//				if(overlapX >= overlapY)
//				{
//					if(vy > 0)
//					{
//						side = COLISION_TOP;
//						s1.setY(s1.getY() + overlapY);
//                                         }
//					else
//					{
//						side = COLISION_BOTTOM;
//                                                s1.setY(s1.getY() - overlapY);
//					}
//				
//				}
//				else
////				if( overlapX < overlapY )
//                                {
//					
//					if( vx > 0 )
//					{
//						side = COLISION_LEFT;
//						s1.setX( s1.getX() + overlapX );
//					}
//					else
//					{
//						side = COLISION_RIGHT;
//						s1.setX(s1.getX()-overlapX);
//					}
//				
//                                }//
//		
//                        }//height
//			
//		}//width
//		
//		return side;
//		
//	}//colision con rectangulos
//        
//        
//        
//        /**
//         * is used to make tile collisions between sprite and colliders used as tiles
//         * @param spr
//         * @param colliderList contains list of colliders used as tiles ( or solid tiles)
//         * @return 
//         */
//        public boolean tileCollision( Sprite spr, List<Collider> colliderList )
//        {
//          return null !=  colliderList.stream().filter( col ->
//            {
//                
//                return !rectangleColision(
//                        spr,
//                        col.getCx(),
//                        col.getCy(),
//                        col.getCw(),
//                        col.getCh() ).equals( COLISION_NONE );
//                
//            }).findFirst().orElse( null );
//         // return ( null != cc);
//        }//
//        
//        
//        
//        /**
//         * basic tile collision, will stop the sprite when colliding with the tiles
//         * @param s1
//         * @param x2
//         * @param y2
//         * @param w2
//         * @param h2
//         * @return 
//         */
//        public String tileCollision2(
//                Sprite s1,
//                float x2, float y2, float w2, float h2)
//	{            
//
//                String side = COLISION_NONE;
//            
//              float vx = (s1.getX()+s1.getCollider().getHalfWidth()) - ( x2 + (w2/2) );
//              float vy = (s1.getY()+s1.getCollider().getHalfHeight()) - ( y2 + (h2/2) );
//                
//              float combinedHalfWidth = 
//                        getCombineHalf( s1.getCollider().getCw(), w2 );
//		
//              float combinedHalfHeight = 
//                        getCombineHalf( s1.getCollider().getCh(), h2 );
//
//            if( Math.abs( vx ) < combinedHalfWidth )
//            {
//                if( Math.abs( vy ) < combinedHalfHeight )
//                {
//                    //ya hay colision
//                    float ovX = combinedHalfWidth - Math.abs(vx);
//                    float ovY = combinedHalfHeight - Math.abs(vy);
//                    
//                    
//                    if( ovX >= ovY )
//                    {
//                                if( vy > 0 )
//                                {
//                                        side = COLISION_TOP;
//                                        s1.setY( s1.getY() + ovY );
//                                 }
//                                else
//                                {
//                                        side = COLISION_BOTTOM;
//                                        s1.setY( s1.getY() - ovY );
//                                }
//                    }//
//                    else
//                    {
//                                if( vx > 0 )
//                                {
//                                        side = COLISION_LEFT;
//                                        s1.setX( s1.getX() + ovX );
//                                }
//                                else
//                                {
//                                        side = COLISION_RIGHT;
//                                        s1.setX( s1.getX() - ovX );
//                                }
//                    
//                    
//                    }//
//              
//                }//on Y
//            }//on X
//            
//            return side;// COLISION_NONE;
//        }
//        
//        
//        
//        
}//
