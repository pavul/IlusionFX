package com.bitless.collision;

import com.bitless.cfg.CollisionType;

/**
 * @TODO CHECK and TEST GETCENTERX and GETCENTERY
 *this is an entity that will be used by all sprites/backgrounds/tiles
 * to collide with each other
 * @author pavulzavala
 * @deprecated 
 */
public class Collider 
{
    
    private CollisionType colType;
    
    private  float cx;
    private  float cy;
    private  float cw;
    private  float ch;
    
    public Collider(float cx, float cy, float cw, float ch, CollisionType colType )
    {
        setColliderBounds( cx, cy, cw, ch, colType );
    }//
    
   /**
    * this method is used to change collider bounds in run time, this is also
    * used on constructor to set initial bounds
    * @param cx collider X
    * @param cy collider Y
    * @param cw collider Width
    * @param ch Collider Height
    * @param colType 
    */
    public final void setColliderBounds(  float cx, float cy, float cw, float ch, CollisionType colType )
    {
        this.cx = cx;
        this.cy = cy;
        this.cw = cw;
        this.ch = ch;
        this.colType = colType;
    }
    
    /**
     * return centerX of collider
     * @return 
     */
//    @Override
//    public float getCenterX() {
//        return (this.cw / 2);
//    }

    /**
     * return centerY of collider
     * @return 
     */
//    @Override
//    public float getCenterY() {
//        return (this.ch / 2);
//    }

    /**
     * return cw/2
     * @return 
     */
//    @Overr

    /**
     * return ch/2
     * @return 
     */
//    @Override
//    public float getHalfHeight() {
//        return this.ch / 2;
//    }
//    
//    //geters and setters
//
//    public CollisionType getColType() {
//        return colType;
//    }
//
//    public void setColType(CollisionType colType) {
//        this.colType = colType;
//    }
//
//    public float getCx() {
//        return cx;
//    }
//
//    public void setCx(float cx) {
//        this.cx = cx;
//    }
//
//    public float getCy() {
//        return cy;
//    }
//
//    public void setCy(float cy) {
//        this.cy = cy;
//    }
//
//    public float getCw() {
//        return cw;
//    }
//
//    public void setCw(float cw) {
//        this.cw = cw;
//    }
//
//    public float getCh() {
//        return ch;
//    }
//
//    public void setCh(float ch) {
//        this.ch = ch;
//    }
    
    
}//
