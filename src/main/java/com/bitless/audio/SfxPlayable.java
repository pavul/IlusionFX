package com.bitless.audio;

/**
 * this interface is used to provide play sound effect method
 * @author PavulZavala
 */
@FunctionalInterface
public interface SfxPlayable 
{
    public void playSfx( String url );
    
}//
