/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bitless.audio;

/**
 *used to stop any music or sound effect
 * @author PavulZavala
 */
@FunctionalInterface
public interface Stopable 
{
    
    public void stop();
    
}
