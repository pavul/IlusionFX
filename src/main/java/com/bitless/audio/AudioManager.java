
package com.bitless.audio;

import com.bitless.cfg.Config;
import com.bitless.util.MathUtil;
import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import javafx.scene.media.AudioClip;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;

/**
 * this class is in charge to play background music (.mp3)
 * and sound effects (.wav), there is volume variable for music 
 * and another volume for sound effects, you can retrieve mediaPlayer instance
 * to have better control over all properties, this class only provide the
 * basic functionality in order to play background music for a level
 * @author PavulZavala
 */
public class AudioManager implements MusicPlayable, SfxPlayable, Muteable
{
    /**
     * to andle music volume
     * 1 = max volume
     * 0 = mute
     */
    private float musicVolume;

    /**
     * to handle sound effects volume
     * 1 = max volume
     * 0 = mute
     */
    private float sfxVolume;
    
    private MediaPlayer mediaPlayer;
    //store all songs of the level

    private boolean autoPlay;
    
    private int cycleCount;
    
    /**
    * list that contains all sound effects for a game
    */
    private Map<String, AudioClip> sfxList; 
    
    ExecutorService sfxPool;
       
    
    /**
     * this constructor set the properties to use by MediaPlayer:
     *  musicVolume = 1;
        sfxVolume = 1;
        autoPlay = true;
        cycleCount = mediaPlayer.INDEFINITE;
     * those properties will be applied to an mp3 file when 
     * play method is executed.
     * You can set those properties again once the instance is created
     * and before play the music file.
     */
    public AudioManager() 
    {
        musicVolume = 1;
        sfxVolume = 1;
        autoPlay = true;
        cycleCount = MediaPlayer.INDEFINITE;
        sfxList = new HashMap<>();
        sfxPool = Executors.newFixedThreadPool( 4 );
        
        
    }//

    
    
    /**
     * plays an mp3 file passed in path argument
     * @param path 
     */
    @Override
    public void playMusic( String path ) 
    {
        // this was the complete url that file.touri.tostring must be seeking 
        //file:/C:/Users/USERNAME/Desktop/ilusionFXGame/build/resources/main/

        Media media = new Media( new File( Config.BASEPATH + path ).toURI().toString() ) ;
        mediaPlayer = new MediaPlayer( media );
        mediaPlayer.setVolume( musicVolume );
        mediaPlayer.setAutoPlay( autoPlay );
        mediaPlayer.setCycleCount( cycleCount );
        mediaPlayer.play();
    }//

    /**
     * use this function to mute current media player
     */
    @Override
    public void mute( boolean mute ) 
    {
     mediaPlayer.setMute( mute );
    }//
    
    
    
    
    public void loadSfx(final String id, String path )
    {
       AudioClip ac = new AudioClip( new File( Config.BASEPATH + path ).toURI().toString() );
       sfxList.put( id , ac );
    }//
    
    
    /**
     * this methods play the specified sound effect
     * @param id 
     */
    @Override
    public void playSfx( final String id )
    {
        Runnable play = () -> 
        {
            sfxList.get( id ).play( sfxVolume );
        };
        
    sfxPool.execute( play );
    }//
    
    
    /**
     * this set a new thread pool with the number of threads specified
     * NOTE: test if this changes if there is a sound currently playing
     * @param threads 
     */
    public void setSfxPool( int threads )
    {
    sfxPool = Executors.newFixedThreadPool( threads );
    }
    
    
    /**
     * remove thread pool used for sound effects
     */
    public void sfxShutdown()
    {
    sfxPool.shutdown();
    }
    
    
    
    /**
     * GETTERS & SETTERS
     */
    
    
    
    
    /**
     * returns current MediaPlayer 
     * @return 
     */
    public MediaPlayer getMediaPlayer() 
    {
        return mediaPlayer;
    }

    public Map<String, AudioClip> getSfxList() {
        return sfxList;
    }
    
    

    public float getMusicVolume() 
    {
        return musicVolume;
    }

    
    /**
     * set the music volume
     * @param musicVolume 
     */
    public void setMusicVolume(float musicVolume) 
    {
        //volume should be between 1 and 0, hence value will be clamped
         musicVolume = MathUtil.clamp( musicVolume, 0, 1);
         mediaPlayer.setVolume(musicVolume);
    }

    public float getSfxVolume() 
    {
        return sfxVolume;
    }

    /**
     * set the sound effects volume
     * every time a sfx is plaid will be with this volume
     * @param sfxVolume 
     */
    public void setSfxVolume(float sfxVolume) 
    {
        this.sfxVolume = MathUtil.clamp( sfxVolume, 0, 1);
    }

    public boolean isAutoPlay() {
        return autoPlay;
    }

    public void setAutoPlay(boolean autoPlay) {
        this.autoPlay = autoPlay;
    }

    public int getCycleCount() {
        return cycleCount;
    }

    public void setCycleCount(int cycleCount) {
        this.cycleCount = cycleCount;
    }


  
    
    
    
    
}//
