package com.bitless.audio;

/**
 * this interface is used t
 * @author PavulZavala
 */
@FunctionalInterface
public interface MusicPlayable 
{
    public void playMusic( String url );
    
}//
