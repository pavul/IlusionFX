package com.bitless.audio;

/**
 *
 * @author PavulZavala
 */
@FunctionalInterface
public interface Muteable 
{
    public void mute( boolean mute );
    
}//
