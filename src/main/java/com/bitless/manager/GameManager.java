package com.bitless.manager;

import com.bitless.cfg.Config;
import com.bitless.ntfc.Stopable;
import com.bitless.ntfc.Startable;
import com.bitless.level.BaseLevel;
import com.bitless.ntfc.GameResizable;
import com.bitless.util.GameUtil;
import javafx.animation.AnimationTimer;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.text.Font;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.StageStyle;


/**
 *this is the main manager for the game, 
 * it render, update, play songs, and have inside all levels of the game
 * @author pavulzavala
 */
public class GameManager  
    implements Startable, Stopable, GameResizable
{
   
    /**
     * this renderer is used to render the whole game
     */
    public static final Canvas RENDERER = new Canvas();
    
    /**
     * main thread
     */
    private final AnimationTimer gameThread;
    
    /**
     * mainRenderer.save & restore useful to apply effects and rotation
     */
//    private GraphicsContext mainRenderer;//main renderer
  
    /**
     * used to scale screen/level of game, this 
     * cannot be used inside render/update loop
     */
    private float xScale = 1f, yScale = 1f;
    
    //gamemanager should handle fonts
    private Font font;
    
    protected BaseLevel currentLevel;
   
    private long pastTick;
   
    private float delta;
    
    private Stage stage;
    
    private boolean isFullScreen;
    
    private int screenWidth;
    private int screenHeight;
    
    @Deprecated
    private boolean isResizable;
    
    /**
     * to know if this will be used as game server
     * if is true, there wont be any display, hence
     * render method will be ignored, all the logic of
     * a game should be in update method
     */
    private boolean server;
    
    
    /**
     * 
     * @param screenWidth
     * @param screenHeight
     */
    public GameManager( int screenWidth, int screenHeight )
    {
        this.screenWidth = screenWidth;
        this.screenHeight = screenHeight;
        
        //setting widht and height for both renders
        RENDERER.setWidth( screenWidth );
        RENDERER.setHeight( screenHeight );
        
        server = false;
        /**
         * getting the renderer and
         * seting the initial scale of the game which should be 1:1
         */
        getRender().scale( xScale, yScale );
        
        
        gameThread = new AnimationTimer() 
        {

            /**
             * main thread proccess or game process
             * @param now 
             */
            @Override
            public void handle( long now )
            {
                //not to run if level is nul
                if( null == currentLevel ){return;}
                
                //getting delta
                delta = ( float ) ( ( now - pastTick ) / 1e9 ) ;
                
               currentLevel.update( delta );
               
               //preRender & postRender are layer to draw the game in screen, 
               //preRender is used to draw background and foreground,
               //while postRender to display HUD, transitions or layer effects
               
               //not render graphics if this is a game server
               if( !server )
                    currentLevel.render( getRender() );
               
               pastTick = now;
                
            }//
            
        };
            
    }//const 1
    
    
//    /**
//     * this method render all the game at the current level
//     */
//    @Override
//     public final synchronized void render()
//    {
//        
//    }//
      
     
//     /**
//      * in this method level update is called
//     * @param delta
//      */
//    @Override
//    public final synchronized void update( float delta )
//    {
//        
//    }//
       
       
       
    /**
     * this method start gameThread
     * and inits the current game
     * graphics
     * listeners
     * font, etc
     */
    public final synchronized void startGame()
    {
        
        /**
         * those methods request the focus of this application, 
         * this is useful in order to listen for input event for
         * keyboard and mouse, request focus as for the focus to the stage,
         * to make that this node (canvas) needs to be focused true
         */
        RENDERER.setFocusTraversable( true );
        //        RENDERER.setFocused( true );
        RENDERER.requestFocus();

        /**
         * loading & setting the font from Util file
         * font size default is 10, can be changed later in the game
         */
        font =  GameUtil.getFont( Config.FONT_PATH , Config.FONT_SIZE );
        getRender().setFont( font );
        getRender().save();
        
        gameThread.start();
        
    }//
    
    
    /**
     * this method end gameThread and free resources
     */
    @Override
    public void stopGame()
    {
        gameThread.stop();    
    }//
     
    
    /**
     * this take an instance of BaseLevel ( a game level to play )
     * and execute draw and update events to start playing
     * @param level 
     */
    public void loadLevel( BaseLevel level )
    {
        this.currentLevel =  level;
    
        //setting all input implementation of the current level for keyboard and mouse 
        //to this canvas (GameManager) in order to handle input events
        //those methods belong to NODE object
        RENDERER.setOnKeyReleased( currentLevel.keyReleasedControl );
        RENDERER.setOnKeyPressed( currentLevel.keyPressedControl );
        RENDERER.setOnKeyTyped( currentLevel.keyTypedControl );
        RENDERER.setOnMouseClicked( currentLevel.mouseClickedControl );
        RENDERER.setOnMousePressed( currentLevel.mousePressedControl );
        RENDERER.setOnMouseReleased( currentLevel.mouseReleasedControl );
        RENDERER.setOnMouseMoved( currentLevel.mouseMovedControl );
        
        //setting game manager to level
        currentLevel.setGm( this );
        
        //loading any exeternal from level that needs to
        //use some property of game manager, like set new font
        //when starting a new level,changin mainRenderer.scale or other scale
        //property
//        currentLevel.onGameloaded();
        
    }//
    
//    /**
//     * retrieve the GraphicsContext used by GameManager ( Canvas )
//     * @param xScale
//     * @return 
//     * @deprecated update the code this will be removed, use preRenderer / postRenderer instead
//     */
//    public GraphicsContext getG()
//    {return mainRenderer;}
    
    
    /*
     * getters & setters below
     */
  
    public void setxScale(float xScale) {
        this.xScale = xScale;
    }

    public void setyScale(float yScale) {
        this.yScale = yScale;
    }

    public float getxScale() {
        return xScale;
    }

    public float getyScale() {
        return yScale;
    }

    public Stage getStage() {
        return stage;
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    public boolean isFullScreen() {
        return isFullScreen;
    }

 
//    public boolean isResizable() {
//        return isResizable;
//    }
//
//    public void setResizable(boolean isResizable) {
//        this.isResizable = isResizable;
//    }
  
    
    /**
     * this functions resize the stage and canvas depending the height value passer
     * and maintains the aspect ratio defined when the game/level are created
     * @param height 
     */
    private void resizeGame( float height ) 
    {
        //Rectangle2D r2 = Screen.getPrimary().getVisualBounds();
        //System.err.println( r2.toString() );
         
        //we have to get the aspect ratio
        float aspectRatio = (float)currentLevel.getLevelWidth() / (float)currentLevel.getLevelHeight();
        
        //new widht will be screenHeight by original aspect ratio of level
        double fixedWidth = height * aspectRatio;
  
        /**
         * if no camera exists mean this is a simple level, hence
         * we take levelWidth and levelHeight, it will take viewWidth and viewHeight
         * if camera exists
         */
        if( null == currentLevel.getCamera() )
        {
        xScale = ( float ) fixedWidth / ( float )currentLevel.getLevelWidth();
        yScale = ( float ) height /  ( float )currentLevel.getLevelHeight();
        
        stage.setMinWidth( currentLevel.getLevelWidth() );
        stage.setMinHeight( currentLevel.getLevelHeight() );
        }//
        else
        {
        xScale = ( float ) fixedWidth / ( float )currentLevel.getCamera().getViewWidth();
        yScale = ( float ) height / ( float )currentLevel.getCamera().getViewHeight();
        
        stage.setMinWidth( currentLevel.getCamera().getViewWidth() );
        stage.setMinHeight( currentLevel.getCamera().getViewHeight() );
        }//
        
        //set canvas new width and height due scaling
        RENDERER.setWidth( fixedWidth  );
        RENDERER.setHeight( height );
        
        //setting stage new width and height due scaling
        //stage.setMaxWidth(fixedWidth);//  setWidth( fixedWidth );
        //stage.setMaxHeight( height );//  setHeight( r2.getHeight() );
        stage.setWidth( fixedWidth );
        stage.setHeight( height );
        
        //setting game new scale
       
        //to maintain aspect rato if windows resized its needed to
        //restore canvas without applying scale and then save it
        //in order to restore it again
        getRender().restore();
        getRender().save();
        getRender().scale( xScale, yScale );
        
    }//

    
    /**
     * when this function is called, game will be resized when
     * the game window is resized with the mouse, if you click on
     * maximize button it will be set to full screen keeping the aspect ratio
     * <b>
     * this method should be called when before stage.show() or it wont work
     * </b>
     */
    @Override
    public void setGameResizable() 
    {
        stage.widthProperty().addListener( 
                 ( obs, oldVal, newVal )->
                 {
                    resizeGame( (float)stage.getHeight() );
                 });
         
        stage.heightProperty().addListener( 
                 ( obs, oldVal, newVal )->
                 {
                    resizeGame( newVal.floatValue() );
                 });
    }//

    /**
     * the game windows will take all space necessary depending on the main monitor
     * the game runs
     * Every time a game is set to full screen, windows task bar will be removed
     */
    @Override
    public void setFullScreen() 
    {
        stage.initStyle(StageStyle.UNDECORATED);
        resizeGame( (float)Screen.getPrimary().getVisualBounds().getHeight() );
    }

    public int getScreenWidth() {
        return screenWidth;
    }

    public void setScreenWidth(int screenWidth) {
        this.screenWidth = screenWidth;
    }

    public int getScreenHeight() {
        return screenHeight;
    }

    public void setScreenHeight(int screenHeight) {
        this.screenHeight = screenHeight;
    }

    public boolean isServer()
    {
        return server;
    }

    public void setServer(boolean server)
    {
        this.server = server;
    }
    
    
    
    public static GraphicsContext getRender()
    {
    return RENDERER.getGraphicsContext2D();
    }
    
    
    
}//class