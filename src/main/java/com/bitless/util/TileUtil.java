package com.bitless.util;

import com.bitless.graphicentity.Tile;
import java.util.ArrayList;
import java.util.List;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;

/**
 * this class needs to be instantiated due not all levels, need tiles, 
 * however if is static will use memory on the heap once the game is created
 * @author pavulzavala
 */
public class TileUtil
{
    /**
     * this transforms int arrays (used as tile maps) and return a list of tiles on the
     * correct X & Y position with width and height of each tile.
     * <b>
     * all 0 values of the array will be ignored, those are taken as transparent tile on the 
     * tile map
     * </b>
     * @param tileMap tileMap to iterate
     * @param tileWidth
     * @param tileHeigth
     * @param rows
     * @param cols
     * @return list of tiles
     */ //int[] tileMap,
     public static List<Tile> parseTiles( int[] tileMap ,int tileWidth, int tileHeigth, int rows, int cols )
     {
         return (List<Tile>)parse( tileMap, tileWidth, tileHeigth, rows, cols );
     }//
         
//     /**
//      * this will parse the tileColliderMap to return a list of solid list which can be used
//      * to make collisions
//      * @param tileMap
//      * @param tileWidth
//      * @param tileHeigth
//      * @param rows
//      * @param cols
//      * @return 
//      * @deprecated 
//      */
//     public  List<Collider> parseCollider(  int[] tileMap ,int tileWidth, int tileHeigth, int rows, int cols )
//     {
//      return (List<Collider>) this.parse( tileMap, tileWidth, tileHeigth, rows, cols, false );
//     }
         
         
    /**
    * this method takes a list of tiles, to print in the screen
     * @param g main renderer
     * @param tileFrames contains different tiles images to draw
     * @param tileList all the tiles that must be drawn
    */
    public static void drawTiles( GraphicsContext g, Image[] tileFrames, List<Tile> tileList)
    {
        tileList.forEach( tile -> 
        {
            // System.err.println(tile);
            g.drawImage( tileFrames[ tile.getIndex() ], tile.getX() , tile.getY() );
        });
        
    }//
    
    
    /**
     * this creates a list of tiles with the corresponding index image position, 
     * x, y, width and height
     * 
     * this method iterate over tileMap (int array) and each value of the array
     * is an image index, those tiles can be used to draw an entire level, the first
     * tile should be transparent and with index 0, that way this method will be ignoring it
     * and will make an smaller list.
     * it can be also used to create a tile map to specify which tiles are solid or where
     * you can have collisions with other objetcs.
     * 
     * @param tileMap
     * @param tileWidth
     * @param tileHeigth
     * @param rows
     * @param cols
     * @param isTile 
     * @return 
     */
    private static List parse( int[] tileMap ,int tileWidth, int tileHeigth, int rows, int cols )
    {
    
        List<Tile> tileList = new ArrayList<>();
           
        int tileMapIndex = 0;
        
        for( int i = 0; i < rows; i++ )
            {
                int tiley = i * tileHeigth;
            
                    //for de columnas
                    for( int j = 0 ; j < cols ; j++ )
                    {
                        
                        int tilex = j * tileWidth;
                      
                        //value of tileMap, can be image or solid tile
                        int tileFrame = tileMap[ tileMapIndex ];
                       
                        tileMapIndex++;
                         
                               //all tiles with 0 values are ignored so they can be used to sort
                               //other tiles used as sprite positions, etc.
                               if( tileFrame == 0 ) continue;
                                
                               Tile t = new Tile();
                               t.setX( tilex );
                               t.setY( tiley );
                               t.setW(tileWidth);
                               t.setH(tileHeigth);
                               t.setIndex( tileFrame );    
                               tileList.add( t );//tile added
                    }//j
            } //i       

        return tileList;
    }//
    
}//class

