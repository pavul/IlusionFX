
package com.bitless.util;

import com.bitless.manager.GameManager;
import java.io.File;
import java.io.IOException;
import java.util.UUID;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.Image;
import javafx.scene.image.PixelReader;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;
import javax.imageio.ImageIO;
import static com.bitless.manager.GameManager.RENDERER;

/**
 *
 * @author PavulZavala
 */
public class ImageUtil 
{
    
    /**
     * this method return one image from resource folder
     * @param pathFile
     * @return 
     */
    public static Image getImage( String pathFile )
    {
        return new Image( pathFile );   
    }//
    
   /**
    * this function converts an image strip into an array of images
    * @param frameNumber
    * @param frameWidth
    * @param frameHeight
    * @param pathToFile
    * @return 
    */
    public static Image[] getImageFrames( int frameNumber, int frameWidth, int frameHeight,  String pathToFile )
    {
       
        Image[] imageFrames =  new Image[ frameNumber ];
        
        //img that contains all frames
        Image sourceImg = new Image( pathToFile );
        PixelReader pr =  sourceImg.getPixelReader();
        
        for( int i = 0 ; i < frameNumber ; i++ )
        {
        
            WritableImage newFrame = new WritableImage( frameWidth, frameHeight );
        
            PixelWriter pw = newFrame.getPixelWriter();
            
            for( int readY = 0 ; readY < frameHeight; readY++ )
            {
            
                int ww = ( frameWidth * i);
                for( int readX = ww; readX < ww + frameWidth; readX++ )
                {
                    //get pixel at X & Y position
                    Color color = pr.getColor( readX, readY );
                    
                    //set pixel to writableimage through pixel writer
                    pw.setColor(readX - ww, readY, color);
                    
                }//X
                
            }//Y
            
            //finally new image is stored
            imageFrames[ i ] = newFrame;
        }//
        
    return imageFrames;
    }//
    
    
    /**
 * this takes an snapshoot of the current game and is stored on the 
 * root where the game resides in PNG format
 * @param gm GameManager( canvas )
 * @param width width of the image
 * @param height height of the image
     * @throws java.io.IOException
 */
public static void takeSnapshot(GameManager gm, int width, int height)
                                                            throws IOException
{
      WritableImage wim = null;//new WritableImage(width, height);
    
      String fileName = UUID.randomUUID().toString();
   
      File file =  new File( fileName+".png" );
      wim = RENDERER.snapshot( null, null );
      ImageIO.write( SwingFXUtils.fromFXImage( wim ,null ), "png", file );
  
}//
    
}
