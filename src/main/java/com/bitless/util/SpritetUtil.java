package com.bitless.util;


import com.bitless.graphicentity.Sprite;
import com.bitless.cfg.Config;
import com.bitless.graphicentity.BaseSprite;
import com.sun.javafx.scene.control.skin.VirtualFlow;
import java.util.List;

/**
 * @TODO look for sprite in specific range and radius, its like
 * spriteneares but with some range
 * this class contain util method for movements or irregular movements
 * like move to certain angle, orbit to certain point
 * @author PavulZavala
 */
public class SpritetUtil 
{
    /**
     * this is going to set the speed on X e Y coordinates pointing to
     * coords toX & toY
     * @note to make this work moveXSpd and moveYSpd needs to be called in update method due
     * this function only set the new speed in X & Y coordinates
     * @param spr
     * @param toX must be x + centerX if towards an sprite
     * @param toY must be y + cemterY if towards an sprite
     * @param atSpeed 
     */
    public static void moveTo( Sprite spr, float toX, float toY, float atSpeed )
{
    if( atSpeed > 0 )
    {
        //OLD CODE, THIS WORKS
        float vx = toX - spr.getX() + spr.getW()/2;
        float vy = toY - spr.getY() + spr.getH()/2;
//        float mag = ( float ) Math.sqrt( ( vx * vx ) + ( vy * vy )  );
//        
//        spr.setSpdX( ( vx / mag ) * atSpeed );
//        spr.setSpdY( ( vy / mag ) * atSpeed );
        
        
//         float vx =
//                 CollisionUtil.getDistance( spr.getX(), spr.getW()/2, toX, 0 );
//        float vy =
//                CollisionUtil.getDistance( spr.getY(), spr.getH()/2, toY, 0 );
        
        float mag = ( float ) Math.sqrt( ( vx * vx ) + ( vy * vy )  );
        
        spr.setSpdX( ( vx / mag ) * atSpeed );
        spr.setSpdY( ( vy / mag ) * atSpeed );
    }
    
}//
    
    /**
     * return the angle in degrees between x & y relative to sprite
     * center point, this can be used to rotate an sprite.
     * if x or y aren't the center of sprite result can be
     * inaccuarate
     * @param spr
     * @param x must be x + centerX if towards an sprite
     * @param y must be x + centerX if towards an sprite
     * @return 
     */
    public static float getAngle( Sprite spr, float x, float y )
    {
      //OLD AND WORKS
          float vx = x - (float)( spr.getX()+spr.getW() / 2 );
          float vy = y - (float)( spr.getY()+spr.getH() / 2 );
    
    //new util function to get distance
//    float vx = CollisionUtil.getDistance( spr.getX(), spr.getW()/2, x, 0 );
//    float vy = CollisionUtil.getDistance( spr.getY(), spr.getH()/2, y, 0 );
        
    float angle =  (float)( Math.atan2( vy , vx ) * Config.RADIAN );
    
    if( angle < 0 )
    {
     angle += Config.CIRCUMFERENCE_GRADES;
    }
    
    return angle;
    }//
    
    
    
    /**@IMPORTANT test this properly
     * this method gets the sprite that is near spr
     * x y w h  are the coordinates of the area of the visible level, this function
     * does not check sprites that are outside of the view
     * @param spr
     * @param spriteList list of sprites to get the neares, usually enemies
     * @param x
     * @param y
     * @param w
     * @param h
     * @return nearest sprite from spriteList to spr
     */
    public static BaseSprite spriteNearest(Sprite spr, 
            List<BaseSprite> spriteList, float x, float y, float w,float h)
    {
        if( spriteList.isEmpty() ) return null;
        
        //only sprites that are inside the view are stored in nearlist
        //List<Sprite> nearList = new ArrayList<>();
        
        float minValue = -1;
        int pos = 0;
        
        for( int i = 0 ; i < spriteList.size() ; i++)
        {
            BaseSprite s = spriteList.get( i );
            
            //select only the sprites that are inside the visible area of level
//            if( CollisionUtil.isInside( 
//                    s.getX(), s.getY(), s.getW(), s.getH(), //sprite
//                    x, y, w, h     ) //level area       
                    
                    if( s.getX() >= x && s.getX()+s.getW() <= x+w && 
                        s.getY() >= y && s.getY()+s.getH() <= y+h )
            {
                
//                OLD AND WORKS
                float vx = (s.getX()+s.getW()/2 ) - (spr.getX() + spr.getW()/2 );
                float vy = (s.getY()+s.getH()/2 ) - (spr.getY() + spr.getH()/2 );
                
                float mag = ( float )Math.sqrt( (vx * vx) + (vy * vy) );

                //new util function to get magnitude
//                float mag = CollisionUtil.getMagnitude(
//                        spr.getX(), spr.getW()/2, spr.getY(), spr.getH()/2, 
//                        s.getX(), s.getW()/2, s.getY(), s.getH()/2 );
                
                //get the minor value of mag, means nearest and save it
                if( mag < minValue || minValue == -1 ) //if minvalue != -1
                {
                minValue = mag;
                pos = i;
                }   
            }
        }//for
        
        //the position with minimum mag value is the neares sprite
      return spriteList.get( pos );
    }//
    
    
    
   /**
    * moves the sprite to the direction specified by sprite angle variable
    * <b>@note: this function will set new speed to sprite spdx and spdy variables</b>
    * @param spr this will be moved depending the angle defined in 'angle' param
    * @param angle this is the angle to move the sprite on that direction
    * @param atSpeed speed to set to the sprite
    * @param setAngle if true, this will be set to sprite.angle variable to rotate the sprite
    */
    public static void moveToAngle( Sprite spr, float angle, float atSpeed, boolean setAngle )
    { 
        if( setAngle )
        { 
        spr.setAngle( angle );
        }
        
        float radAngle = (float)Math.toRadians( angle );
        float spdx = atSpeed * (float)Math.cos( radAngle ); 
        float spdy = atSpeed * (float)Math.sin( radAngle );

        spr.setSpdX( spdx );
        spr.setSpdY( spdy );
    }//
    
    
    /**
     * this looks for a specific sprite with the value of id 
     * <b> NOTE: there is a static property in Sprite class called SpritePool,
     * each time a sprite is created is added to that SpritePool, this method uses
     * this spritepool to retrieve a list of sprites with some label in common
     * </b>
     * @param spritesToSearch
     * @param id
     * @return return the sprite if found
     */
    public static BaseSprite 
        getSpritesById( List<BaseSprite> spritesToSearch, int id )
    {
        for( BaseSprite spr: spritesToSearch)
        {
            if( spr.getId() == id )
            {
                return spr;
            }
        }
//       return spritesToSearch.stream()
//                           .filter( spr -> spr.getId() == id ).findFirst().get();
        return null;
    }
    
    
    /**
     * returns a list with all sprites containing specified label,
     *<b> NOTE: there is a static property in Sprite class called SpritePool,
     * each time a sprite is created is added to that SpritePool, this method uses
     * this spritepool to retrieve a list of sprites with some label in common
     * </b>
     * @param spritesToSearch
     * @param label
     * @return 
     */
    public static List<BaseSprite>  getSpritesByLabel( List<BaseSprite>  spritesToSearch, String label )
    {
        List<BaseSprite> foundSprites = new VirtualFlow.ArrayLinkedList<>();
        
        for(BaseSprite spr: spritesToSearch)
        {
            if( spr.getLabel().equals( label ) )
            {
            foundSprites.add( spr );
            }
        }
        return foundSprites;
//        return spritesToSearch.stream().
//                    filter( spr -> spr.getLabel().equals( label ) )
//                                            .collect(Collectors.toList());
    }//
    

    /**
     * this function return the distance between sprite and point
     * defined by X & Y, i suggest point X & Y must be center point
     * of the figure or sprite to get the distance.
     * @param spr
     * @param x
     * @param y
     * @return distance of the vector between sprite and point, this value is not pixel length
     */
    public static float getDistance(Sprite spr, float x, float y)
    {
//    return CollisionUtil.getMagnitude(spr.getX(), spr.getW()/2, spr.getY(), spr.getH()/2, 
//                                        x, 0, y, 0);
        float vx = x - spr.getX() + spr.getW()/2;
        float vy = y - spr.getY() + spr.getH()/2;
        return ( float ) Math.sqrt( ( vx * vx ) + ( vy * vy )  );

    }//
    
    
    /**
     * this function return the distance between two sprites
     * the distance is retrieved from center point of the two sprites
     * @param spr  
     * @param s  
     * @return distance of the vector between Sprite 1 and Sprite 2, this value is not pixel length
     */
    public static float getDistance(Sprite spr, Sprite s)
    {
    return  getDistance(s, spr.getX() + spr.getW()/2, spr.getY() + spr.getH()/2 );
//            ) (spr.getX(), spr.getW()/2, spr.getY(), spr.getH()/2, 
//                                      s.getX(), s.getW()/2, s.getY(), s.getH()/2 );
    }//
    
    
    
}//class
