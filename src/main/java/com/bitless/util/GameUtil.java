package com.bitless.util;

import com.bitless.level.BaseLevel;
import com.bitless.manager.GameManager;
import javafx.application.Platform;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.text.Font;
import javafx.stage.Stage;

/**
 *@TODO 
 * getTextWidth
 * getRandomCOlor
 * getProperties // to get all values from property files
 * save Image as PNG of the canvas or game
 * @author pavulzavala
 */
public final class GameUtil 
{
    
    /**
     * this function is intended to use in the Application class, 
     * specifically inside method start ,to set the scene > stage and 
     * the proper canvas where the game will run, this is also
     * the basic boiler plate to start the game to not rewrite all
     * the code again
     * @param stage
     * @param entryLevel
     * @param gameScreenWidth
     * @param gameScreenHeight 
     */
    public static void startGame( Stage stage, BaseLevel entryLevel, int gameScreenWidth, int gameScreenHeight )
    {
        //this is where game is created/initiated
        GameManager gm = new GameManager( gameScreenWidth, gameScreenHeight );
        
        //set the stage is needed if you want to resize the game or make other config...
        gm.setStage( stage );
        
        gm.loadLevel( entryLevel );
        
        gm.setGameResizable();
        
        /**
         * IMPORTANT!, below code is the creation of the scene where
         * the GameManager will be mounted in order to initiate the game
         */
        if( !gm.isServer() )
        {
        Group root = new Group();
                
        Scene scene = new Scene( root, gm.getScreenWidth(), gm.getScreenHeight() );
        
        root.getChildren().add(GameManager.RENDERER );
         
        stage.setScene( scene );
        
        stage.show();
        }
              
        
        //make game start
        gm.startGame();
          
    }//startGame
    
    
/**
 *  this function loads a new font specified by path, size of the font
 * this font should be set to graphicsContext.setFont( x )
* @param pathToFont
* @param fontSize
 * @return 
 */
public static Font getFont( String pathToFont, int fontSize )
{
  //using the same instance PATH to get 'getClass and getResource'
  return Font.loadFont( pathToFont.getClass().getResource( pathToFont ).toExternalForm() , fontSize );
}//setfont
    
/**
 * this function is used to close any game when used,
 * specifically closes the current application
 */
public static void closeGame()
{
Platform.exit();
System.exit( 0 );
}


/**@todo possible move to other more accuarate class
 * when used this method will shakeIntensity the screen with
 shakeIntensity value, if shakeIntensity value is <= 0 then will
 * be set to 5
 * @param shakeIntensity 
 */
public static void screenShake( int shakeIntensity )
{
 
    if( shakeIntensity <= 0 )
    { shakeIntensity = 5; }
    
    GameManager.getRender().restore();
    GameManager.getRender().save();
    GameManager.getRender().translate( 
            MathUtil.getRandomRange(shakeIntensity, shakeIntensity ),
            MathUtil.getRandomRange(shakeIntensity, shakeIntensity ) );
    
}
    


}//
