/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bitless.util;

/**
 *
 * @author PavulZavala
 */
public class MathUtil 
{
    
    /**
 * returns X position of the vector determined by length and angleDirection
 * <b>NOTE: you must add to this value to originX where will be added this vector
 * for example sprite.getX()+lengthdirX( 100, 45 )</b>
 * @param length
 * @param angleDirection
 * @return 
 */
public static float lengthdirX( float length, float angleDirection )
{
return (float)Math.cos( angleDirection * (Math.PI/180) ) * length;
}


/**
 * returns Y position of the vector determined by length and angleDirection
 * <b>NOTE: you must add to this value to originY where will be added this vector
 * for example sprite.getY()+lengthdirY( 100, 45 )</b>
 * @param length
 * @param angleDirection
 * @return 
 */
public static float lengthdirY( float length, float angleDirection )
{
return (float)Math.sin( angleDirection * (Math.PI/180) ) * length;
}


/**
 * if value is mayor than max value, max value will be returned,
 * if value is minor than min value, min value will be returned,
 * if value is in range between mayor and minor values, then 
 * value will be returned
 * @param value
 * @param minValue
 * @param maxValue
 * @return value will never falls below minValue or goes over maxValue
 */
public static float clamp( float value, float minValue, float maxValue )
{
    // value must be between minValue and maxValue
    return (value > maxValue) ? maxValue : (value < minValue) ? minValue : value;
}

/**
 * this function creates linear interpolation depending on argument t,
 * which is the percentage of v0 until v1
 * @example lerp(0, 10, 0.5): will return 5.0, because is the middle ( 0.5 ) between 0 and 10
 * @param v0
 * @param v1
 * @param t
 * @return 
 */
public static float Lerp(float v0, float v1, float t)
{
    return (1-t)*v0 + t*v1;
}

/**
 * generates and return a number between min and max values
 * it can be casted to int if you need an int number, you can
 * also Math.floor or Math.ceil the number, so it can be used
 * to choose a number between min and max range
 * <b>
 * NOTE: minimun number must be positive, this function will take
 * that as negative for example to return a number between range -2 to 2,
 * should be generatRandomPositiveNegitiveValue( 2, 2 );  
 * </b>
 * @param min
 * @param max
 * @return 
 */
public static float getRandomRange( float min, float max )
{
    return -min + ( float )( Math.random() * (  ( max - ( -min )  ) + 1 ) );
}
    
    
  /**
     * this function help to resolve proporsional ammounts,
     * util if you want to get hp ammount, with a fixed width 
     * healt bar
     * 
     * example:
     * a -- > b
     * c --> x
     * -----------------
     * 50 --> 100%
     * 25 --> ?
     * @param a
     * @param b
     * @param c
     * @return the proportion of the value of C param
     */
    public static float ruleOf3( float a, float b, float  c  )
    {
        return( ( b * c ) / a );
    }//
    
    
    
}//
