/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bitless.util;

import com.bitless.graphicentity.Sprite;
import com.bitless.graphicentity.Tile;

/**
 * this class is a helper to proccess collisions
 * if your game needs basic collision you can instantiate this
 * class, if you pretend to use any other physics framework like 
 * Jbox2d, you can ommit this
 * 
 * @author PavulZavala
 */
public class CollisionUtil 
{
    public   String COLISION_TOP = "TOP";
    public   String COLISION_BOTTOM = "BOTTOM";
    public   String COLISION_LEFT = "LEFT";
    public   String COLISION_RIGHT = "RIGHT";
    public   String COLISION_NONE = "NONE";
        
    
  
    /**
     * return distance betwen 2 points
     * @Note this i a note
     * @param point
     * @param width
     * @param point2
     * @param width2
     * @return 
     */
    public  float getDistance( float point, float width, float point2, float width2 )
    {
     return ( point2 + width2 / 2 ) - ( point + width / 2 );
    }//
    
    
    /**
     * this get the magnitude between 2 points x, y & width, height
     * the process is substract point2 x,y - point x,y coordinates
     * and it uses getDistance function
     * eve if we pass param1 & param2, they will be processes as
     * param2 - param1
     * @param x
     * @param w
     * @param y
     * @param h
     * @param x2
     * @param w2
     * @param y2
     * @param h2
     * @return 
     */
    public  float getMagnitude( float x, float w ,float y, float h,
                                      float x2, float w2 ,float y2, float h2 )
    {
        float vx = getDistance(x, w, x2, w2);
        float vy = getDistance(y, h, y2, h2);
    return ( float ) Math.sqrt( ( vx * vx ) + ( vy * vy ) );
    }//
    
    
//        float vx = toX - spr.getX() + spr.getCollider().getHalfWidth();
//        float vy = toY - spr.getY() + spr.getCollider().getHalfHeight();
//        float mag = ( float ) Math.sqrt( ( vx * vx ) + ( vy * vy )  );
//    
    
    
    
      /**
    * this function is used to combine the half parts from
    * 2 rects, circles or other shapes as well 2 sprites
    * @param half1
    * @param half2
    * this will process something like:
    * float combinedHalfHeight = h / 2 + h2 / 2; 
    * @return 
    */
   public  float getCombineHalf( float half1, float half2 )
   {
   return ( half1 / 2 ) + ( half2 / 2 );
   }
    
   
   /**
    * returns if value1 is minor than value2,
    * if value1 is negative this is changed to positive
    * @param val1
    * @param val2
    * @return 
    */
   public  boolean isOverlaped( float val1, float val2 )
   {
       return ( Math.abs( val1 ) < val2 );   
   }//
    
   /**
    * check whether the point defined by X & Y is inside
    * X2, Y2, W2, H2
    * @param x
    * @param y
    * @param x2
    * @param y2
    * @param w2
    * @param h2
    * @return 
    */
   public  boolean pointCollision( float x, float y, 
                                         float x2, float y2, float w2, float h2 )
   {
       return (x >= x2  &&
               x <= x2 + w2 &&
               y >= y2 &&
               y <= y2 + h2);
   }
   
   /**
    * check whether the point specified by X e Y is inside
    * of Sprite s
    * @param x
    * @param y
    * @param s
    * @return true if point is inside sprite
    */
   public  boolean pointCollision( float x, float y, Sprite s )
   {
       float x2 = s.getX();// + s.getW()/2;
       float y2 = s.getY();// + s.getH()/2;
       float w2 = s.getW();
       float h2 = s.getH();
       
       return pointCollision(  x, y, x2, y2, w2, h2 );
   }
   
   
        /**
         * checks collision between 2 circles
         * WORKS
         * @param cenx center X of circle 1
         * @param ceny center Y of circle 1
         * @param rad radius of circle 1 ( collider.width /2 )
         * @param cenx2 center X of circle 2
         * @param ceny2 center Y of circle 2
         * @param rad2 radius of circle 2 ( collider.width /2 )
         * @return 
         * @usage circleCollision( s.getX + s.getCollider.centerX, s.getCollider.getHalfWidth,
         * s2.getX + s2.getCollider.centerX, s2.getCollider.getHalfWidth )
         */
        public  boolean circleCollision( 
                float cenx,float ceny,float rad,
                float cenx2,float ceny2,float rad2 )
        {
            float magnitude =  getMagnitude(cenx, 0, ceny, 0, cenx2, 0, ceny2, 0);
                    
            //get total radio of two circles
            float totalRadio = rad + rad2;
         
            return magnitude < totalRadio;
            
        }//
        
        
        /**
         * checks collision between Sprite and circles
         * <b>NOTE: if sprite is squared image, radio will be sprite.getW()/2</b>
         * WORKS
         * @param s sprite to check collision 
         * @param cenx center X of circle 
         * @param ceny center Y of circle 
         * @param rad radius of circle 1 ( collider.width /2 )
         * @param fixOverlap if true will prevent two circle colliders from overlap
         * @return 
         * @usage circleCollision( s.getX + s.getCollider.centerX, s.getCollider.getHalfWidth,
         * s2.getX + s2.getCollider.centerX, s2.getCollider.getHalfWidth )
         */
        public  boolean circleCollision( Sprite s ,float cenx,float ceny,float rad, boolean fixOverlap )
        {
             //get circle vectors
            float vx =  ( s.getX() + s.getW()/2 ) - cenx;
            float vy =  ( s.getY() + s.getH()/2 ) - ceny;
            
             //calculate distance between circles
            float magnitude = (float)Math.sqrt( (vx * vx) + (vy * vy) );
           
            //get total radio of two circles
            float totalRadio = s.getW()/2 + rad;
        
            boolean res = magnitude < totalRadio;
           
            /**
             * this will prevent two circles from
             * overlapping
             */
            if( fixOverlap && res )
            {
                float overlap = totalRadio - magnitude;
                float dx = vx / magnitude;
                float dy = vy / magnitude;
                
                s.setX( s.getX() + (overlap * dx)  );
                s.setY( s.getY() + (overlap * dy)  );
            }
            
            return res;
                
        }//
        
        
        /**
         * checks collision between two sprites
         * WORKS
         * @param s sprite to check collision 
         * @param s2 sprite to check collision 
         * @param fixOverlap dont let botch circles overlap
         * @return 
         * @usage circleCollision( s.getX + s.getCollider.centerX, s.getCollider.getHalfWidth,
         * s2.getX + s2.getCollider.centerX, s2.getCollider.getHalfWidth )
         */
          public  boolean circleCollision( Sprite s ,Sprite s2, boolean fixOverlap )
          {
            return circleCollision( 
                    s,
                    s2.getX() + s2.getW()/2,
                    s2.getY() + s2.getH()/2, 
                    s2.getW()/2,
                    fixOverlap );
         }//

          
        /**
         * checks if there is rectangle collision  between 2 rectangles (2 boxes)
         * TESTED AND WORKS
         * @param x
         * @param y
         * @param w
         * @param h
         * @param x2
         * @param y2
         * @param w2
         * @param h2
         * @return 
         */
        public  boolean rectangleColision(
                float x, float y, float w, float h,
                float x2, float y2, float w2, float h2)
	{            

            float combinedHalfWidth = w / 2 + w2 / 2;
            float combinedHalfHeight = h / 2 + h2 / 2;

            return ( Math.abs( getDistance( x, w, x2, w2 ) ) < combinedHalfWidth && 
                     Math.abs( getDistance( y, h, y2, h2 ) ) < combinedHalfHeight  );
          		
	}//colision rectangular
          
          
        /**
         * checks if there is rectangle collision  between 2 Sprites
         * TESTED AND WORKS
         * @param s1
         * @param s2
	 * @return true if there is a collision and false if not
	 * */
	public  boolean rectangleColision(Sprite s1, Sprite s2)
	{
            return rectangleColision( 
                                    s1.getX(),
                                    s1.getY(),
                                    s1.getW(),
                                    s1.getH(),
                                    s2.getX(),
                                    s2.getY(),
                                    s2.getW(),
                                    s2.getH());	
	}//

        
        //TEST THIS WITH GRAVITY, IT SHOULD WORK REMOVING LAST ELSE CODE
        public  String rectangleCollision(float x, float y, float w, float h,
                                         float x2, float y2, float w2, float h2, boolean push )
        {
            String side = COLISION_NONE;
            
            float vx = getDistance( x2, w2, x, w );
            float vy = getDistance( y2, h2, y, h );
            
             float combinedHalfWidth = getCombineHalf( w, w2 );
             float combinedHalfHeight= getCombineHalf( h, h2 );
         
             float vxabs = Math.abs( vx );
             float vyabs = Math.abs( vy );
             
             //if true then there is a collision
             if( vxabs < combinedHalfWidth && vyabs < combinedHalfHeight )
             {
             
                float overlapX = combinedHalfWidth  - vxabs; 
                float overlapY = combinedHalfHeight - vyabs;
                 
                if( overlapX >= overlapY )
                {
                        if( vy > 0 )
                        {
                            side = COLISION_TOP;
                            if( push ) y2 += -1; //s2.setY( s2.getY()-1 );
//                                if( push )s2.setY( s2.getY()-1 );
                             y += overlapY; //s1.setY(s1.getY() + overlapY);
                         }
                        else
                        {
                            side = COLISION_BOTTOM;
                            if( push )y2 += 1;//  s2.setY(s2.getY()+1);
//                                if( push )s2.setY(s2.getY()+1);
//                                s1.setY(s1.getY() - overlapY);
                            y -= overlapY;
                        }

                }
                else
                {

                        if( vx > 0 )
                        {
                            side = COLISION_LEFT;
                            if( push )x2+=-1;//  s2.setX(s2.getX()-1);
//                                s1.setX( s1.getX() + overlapX );
                            x += overlapX;
                        }
                        else
                        {
                            side = COLISION_RIGHT;
                            if( push )x2+=1; // s2.setX(s2.getX()+1);
//                                if( push )s2.setX(s2.getX()+1);
//                                s1.setX(s1.getX()-overlapX);
                            x-=overlapX;
                        }

                }//
                
             }//
             return side;
        }//
                
        
        
        /**
         * checks wether there is a collision between two sprites
         *
         * Example:  blockRectangle(spritePlayer, box, true);
         * @param s1
         * @param s2
         * @param push it push is true then sprite1 will push sprite 2
	 * @return true if there is a colision and false if not
	 * 
	 * */
	public  String rectangleColision( Sprite s1, Sprite s2,boolean push )
	{
//		String side = COLISION_NONE;
//                
//                float vx = 
//                        (s1.getX() + s1.getCollider().getHalfWidth() ) - 
//                        (s2.getX() + s2.getCollider().getHalfWidth() );
//                
//                float vy = 
//                        (s1.getY() + s1.getCollider().getHalfHeight() ) - 
//                        (s2.getY() + s2.getCollider().getHalfHeight() );
//	
//                float combinedHalfWidth = 
//                        getCombineHalf( s1.getCollider().getCw(), s2.getCollider().getCw() );
//		
//                float combinedHalfHeight = 
//                        getCombineHalf( s1.getCollider().getCh(), s2.getCollider().getCh() );
//		
//		if( Math.abs( vx ) < combinedHalfWidth )
//		{
//                    
//			if( Math.abs( vy ) < combinedHalfHeight )
//			{
//				float overlapX = combinedHalfWidth - Math.abs( vx );
//				float overlapY = combinedHalfHeight - Math.abs( vy );
//				
//				if( overlapX >= overlapY )
//				{
//					if( vy > 0 )
//					{
//						side = COLISION_TOP;
//						if( push )s2.setY( s2.getY()-1 );
//						s1.setY(s1.getY() + overlapY);
//                                         }
//					else
//					{
//						side = COLISION_BOTTOM;
//						if( push )s2.setY(s2.getY()+1);
//                                                s1.setY(s1.getY() - overlapY);
//					}
//				
//				}
//				else
//				{
//					
//					if( vx > 0 )
//					{
//						side = COLISION_LEFT;
//						if( push )s2.setX(s2.getX()-1);
//						s1.setX( s1.getX() + overlapX );
//					}
//					else
//					{
//						side = COLISION_RIGHT;
//						if( push )s2.setX(s2.getX()+1);
//						s1.setX(s1.getX()-overlapX);
//					}
//				
//                                }//
//		
//                        }//height
//			
//		}//width
//		
//		return side;
		
            
            String side = COLISION_NONE;
            
            //NOTE: CHECK THIS SPRITE2 IS PUT FIRST THEN SPRITE1, OTHERWISE WONT WORK
            float vx = getDistance( s2.getX(), s2.getW(), s1.getX(), s1.getW() );
            float vy = getDistance( s2.getY(), s2.getH(), s1.getY(), s1.getH() );
            
             float combinedHalfWidth = getCombineHalf( s1.getW(), s2.getW() );
             float combinedHalfHeight= getCombineHalf( s1.getH(), s2.getH() );
         
             float vxabs = Math.abs( vx );
             float vyabs = Math.abs( vy );
             
             //if true then there is a collision
             if( (vxabs < combinedHalfWidth) && (vyabs < combinedHalfHeight) )
             {
             
                float overlapX = combinedHalfWidth  - vxabs; 
                float overlapY = combinedHalfHeight - vyabs;
                 
                if( overlapX >= overlapY )
                {
                        if( vy > 0 )
                        {
                            side = COLISION_TOP;
                            if( push )s2.setY( s2.getY()-1 );
                            s1.setY(s1.getY() + overlapY);
                         }
                        else
                        {
                            side = COLISION_BOTTOM;
                           if( push )s2.setY(s2.getY()+1);
                           s1.setY(s1.getY() - overlapY);
                           
                        }

                }
                else
                {

                        if( vx > 0 )
                        {
                            side = COLISION_LEFT;
                            if( push )s2.setX(s2.getX()-1);
                            s1.setX( s1.getX() + overlapX );
                        }
                        else
                        {
                           side = COLISION_RIGHT;
                           if( push )s2.setX(s2.getX()+1);
                           s1.setX(s1.getX()-overlapX);
                        }

                }//
                
             }//
             return side;
            
	}//

        
//        @TODO CREATE TILECOLLISION
         public  String rectangleColision( Sprite s1, Tile s2,boolean push )
	{
//		
            String side = COLISION_NONE;
            
            //NOTE: CHECK THIS SPRITE2 IS PUT FIRST THEN SPRITE1, OTHERWISE WONT WORK
            float vx = getDistance( s2.getX(), s2.getW(), s1.getX(), s1.getW() );
            float vy = getDistance( s2.getY(), s2.getH(), s1.getY(), s1.getH() );
            
             float combinedHalfWidth = getCombineHalf( s1.getW(), s2.getW() );
             float combinedHalfHeight= getCombineHalf( s1.getH(), s2.getH() );
         
             float vxabs = Math.abs( vx );
             float vyabs = Math.abs( vy );
             
             //if true then there is a collision
             if( (vxabs < combinedHalfWidth) && (vyabs < combinedHalfHeight) )
             {
             
                float overlapX = combinedHalfWidth  - vxabs; 
                float overlapY = combinedHalfHeight - vyabs;
                 
                if( overlapX >= overlapY )
                {
                        if( vy > 0 )
                        {
                            side = COLISION_TOP;
                            if( push )s2.setY( s2.getY()-1 );
                            s1.setY(s1.getY() + overlapY);
                         }
                        else
                        {
                            side = COLISION_BOTTOM;
                           if( push )s2.setY(s2.getY()+1);
                           s1.setY(s1.getY() - overlapY);
                           
                        }

                }
                else
                {

                        if( vx > 0 )
                        {
                            side = COLISION_LEFT;
                            if( push )s2.setX(s2.getX()-1);
                            s1.setX( s1.getX() + overlapX );
                        }
                        else
                        {
                           side = COLISION_RIGHT;
                           if( push )s2.setX(s2.getX()+1);
                           s1.setX(s1.getX()-overlapX);
                        }

                }//
                
             }//
             return side;
            
	}//
        
         
         /**
          * this rectangle collision returns the side where s1 is colliding with
          * s2, NOONE if does not collide
          * @param s1
          * @param s2
          * @return 
          */
         public  String rectangleCollision( Sprite s1, Sprite s2 )
         {
         
             String side = COLISION_NONE;
            
            float vx = getDistance( s2.getX(), s2.getW(), s1.getX(), s1.getW() );
            float vy = getDistance( s2.getY(), s2.getH(), s1.getY(), s1.getH() );
            
             float combinedHalfWidth = getCombineHalf( s1.getW(), s2.getW() );
             float combinedHalfHeight= getCombineHalf( s1.getH(), s2.getH() );
         
             float vxabs = Math.abs( vx );
             float vyabs = Math.abs( vy );
         
             if( isOverlaped( vx, combinedHalfWidth ) && isOverlaped( vy, combinedHalfHeight ) )
             {
                 
                 float overlapX = combinedHalfWidth  - vxabs; 
                 float overlapY = combinedHalfHeight - vyabs;
                 if( overlapX >= overlapY )
                {
                        if( vy > 0 )
                        {
                           return side = COLISION_TOP;
                        }
                        else
                        {
                            return COLISION_BOTTOM;
                        }

                }
                else
                {

                        if( vx > 0 )
                        {
                            return COLISION_LEFT;
                        }
                        else
                        {
                            return COLISION_RIGHT;
                          
                        }
                }
                 
             }//
             
             return COLISION_NONE;
         }//
         
         
//        
//        
//        
//        /**
//         * is used to make tile collisions between sprite and colliders used as tiles
//         * @param spr
//         * @param colliderList contains list of colliders used as tiles ( or solid tiles)
//         * @return 
//         */
//        public boolean tileCollision( Sprite spr, List<Collider> colliderList )
//        {
//          return null !=  colliderList.stream().filter( col ->
//            {
//                
//                return !rectangleColision(
//                        spr,
//                        col.getCx(),
//                        col.getCy(),
//                        col.getCw(),
//                        col.getCh() ).equals( COLISION_NONE );
//                
//            }).findFirst().orElse( null );
//         // return ( null != cc);
//        }//
//        
//        
//        
//        /**
//         * basic tile collision, will stop the sprite when colliding with the tiles
//         * @param s1
//         * @param x2
//         * @param y2
//         * @param w2
//         * @param h2
//         * @return 
//         */
//        public String tileCollision2(
//                Sprite s1,
//                float x2, float y2, float w2, float h2)
//	{            
//
//                String side = COLISION_NONE;
//            
//              float vx = (s1.getX()+s1.getCollider().getCenterX()) - ( x2 + (w2/2) );
//              float vy = (s1.getY()+s1.getCollider().getCenterY()) - ( y2 + (h2/2) );
//                
//              float combinedHalfWidth = 
//                        getCombineHalf( s1.getCollider().getCw(), w2 );
//		
//              float combinedHalfHeight = 
//                        getCombineHalf( s1.getCollider().getCh(), h2 );
//
//            if( Math.abs( vx ) < combinedHalfWidth )
//            {
//                if( Math.abs( vy ) < combinedHalfHeight )
//                {
//                    //ya hay colision
//                    float ovX = combinedHalfWidth - Math.abs(vx);
//                    float ovY = combinedHalfHeight - Math.abs(vy);
//                    
//                    
//                    if( ovX >= ovY )
//                    {
//                                if( vy > 0 )
//                                {
//                                        side = COLISION_TOP;
//                                        s1.setY( s1.getY() + ovY );
//                                 }
//                                else
//                                {
//                                        side = COLISION_BOTTOM;
//                                        s1.setY( s1.getY() - ovY );
//                                }
//                    }//
//                    else
//                    {
//                                if( vx > 0 )
//                                {
//                                        side = COLISION_LEFT;
//                                        s1.setX( s1.getX() + ovX );
//                                }
//                                else
//                                {
//                                        side = COLISION_RIGHT;
//                                        s1.setX( s1.getX() - ovX );
//                                }
//                    
//                    
//                    }//
//              
//                }//on Y
//            }//on X
//            
//            return side;// COLISION_NONE;
//        }
//        
//    
         
         
         
         /**
 * method that checks wheter x, y,w,h is inside x2,y2,w2,h2 area, usually to
 * check whether a sprite or image or box is inside a bigger space like, room, camera view
 * pointer mouse coordinates or if a button was taped, etc.
 * it can be used to check if a pixel is inside a rectangle
 * <b>@note: this can be used too, to check wheter some sprite is outside some area, in
 * case we need to reacomodate an sprite that leaves the view</b>
 * @param x
 * @param y
 * @param w
 * @param h
 * @param x2
 * @param y2
 * @param w2
 * @param h2
 * @return 
 */
public  boolean isInside( float x, float y , float w, float h, float x2, float y2 , float w2, float h2 )
{
    return ( x >= x2 && x+w <= x2+w2 && y >= y2 && y+h <= y2+h2 ); 
}
         
         
}//