
package com.bitless.cfg;

/**
 * this only contains constant confguration used by this library
 * NOTE: this is only the string as constant, those paths are not supposed
 * to change
 * @author PavulZavala
 */
public class Config 
{
    
    /*paths of resources*/
    public static final String FONT_PATH = "/com/bitless/font/pressstart2p.ttf";
    
    //default size of font
    public static final int FONT_SIZE = 10;
    
    //this is increased every time an sprite is created
    public static int SPRITE_ID_COUNTER = 1_000;
    
    //this is used to get socket id
    public static int SOCKET_ID_COUNTER = 1_000;
 
    //fixed size for buffers when used with UDP connections
    public static final int UDP_BUFFER_SIZE = 1024;
    
    //variables used on SpriteUtil
    public static final double RADIAN  = ( 180 / Math.PI );
    public static final int CIRCUMFERENCE_GRADES  = 360;
    
    //path to access resoures from .jar
    public static final String BASEPATH = "build/resources/main/";
    
    public static final float COLOR_ADJUST_DISABLED = -5f;
    
    public static final float GAMEPAD_DEFAULT_THRESHOLD = 0.35f;
}
