/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bitless.cfg;

/**
 * this enumerates types of collisions between colliders
 * in this case they are Rectangle & Circle
 * NOTE: at this moment only rectangle is available
 * @author pavulzavala
 */
public enum CollisionType 
{
   RECTANGLE,
   CIRCLE
    
}//
