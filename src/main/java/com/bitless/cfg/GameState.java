/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bitless.cfg;

/**
 * possibles Game States for a Level
 *
 * @author pavulzavala
 */
public enum GameState 
{
 
    LOADING, //cargando
    PLAYING, //jugando
    GAMEOVER, //fin del juego
    COMPLETED, //nivel completado
    PAUSED, //juego pausado
    STOPPED, //juego detenido
    DIALOGUING //personajes en dialogo
    
}
