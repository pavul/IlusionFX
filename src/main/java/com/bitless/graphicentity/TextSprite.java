
package com.bitless.graphicentity;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

/**
 *
 * @author PavulZavala
 */
public class TextSprite extends BaseSprite
{
    private String text;
    private Color textColor;
    private boolean fillText;
    private int maxTextWidth;
    
    public TextSprite( int x, int y, String text)
    {
    super();
    this.x = x;
    this.y = y;
    this.text = text;
    textColor =  Color.BLACK;
    maxTextWidth = 100;
    this.fillText = true;
    }
    
    
    @Override
    public void draw( GraphicsContext g ) 
    {
        
        if( visible )
        {
            
            /**
             * code to execute if the sprite is rotated, scaled or if is
             * transparent
             */
            if( alpha < 1 || angle != 0 || xScale != 1 || yScale != 1 )
            {
            g.save();
               //to make sprite opaque
                g.setGlobalAlpha( alpha );

                g.translate( x + w/2  , y + h/2 );
                g.rotate( angle );
                g.scale( xScale, yScale );
                g.translate( -( x + w/2 ) , -( y + h/2 ) );

                drawText( g );
            g.restore();
            }
            else
            {
            drawText( g );
            }
            
        }//
        
        
        
        
    }

    private void drawText( GraphicsContext g )
    {
        if( fillText )
        {
        g.setFill( textColor );
        g.fillText(text, x, y, maxTextWidth );
        }
        else
        {
        g.setStroke( textColor );
        g.strokeText (text, x, y, maxTextWidth );
        }
    }

    public int getMaxTextWidth() {
        return maxTextWidth;
    }

    public void setMaxTextWidth(int maxTextWidth) {
        this.maxTextWidth = maxTextWidth;
    }

    
    @Override
    public void update(float l)
    {
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Color getTextColor() {
        return textColor;
    }

    public void setTextColor(Color textColor) {
        this.textColor = textColor;
    }

    public boolean isFillText() {
        return fillText;
    }

    public void setFillText(boolean fillText) {
        this.fillText = fillText;
    }
 
}
