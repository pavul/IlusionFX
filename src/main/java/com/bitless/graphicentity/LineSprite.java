package com.bitless.graphicentity;

import com.bitless.ntfc.Drawable;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

/**
 * this class represents a line
 * 
 */
public class LineSprite extends BaseSprite 
                            implements Drawable
{
    
    
    private int lineWidth;
    private Color lineColor;
    
    /**
     * creates a line from point (x, y) until point ( x + width, y + height )
 with a black lineColor and 1 pixel width
     * @param x
     * @param y
     * @param w
     * @param h
     */
    public LineSprite(float x, float y, float w, float h)
    {
    super();
    this.x = x;
    this.y = y;
    this.w = w;
    this.h = h;
    lineColor = Color.BLACK;
    lineWidth = 1;
    }
    
    /**
     * draws the line depending on settings
     * @param g 
     */
    @Override
    public void draw( GraphicsContext g ) 
    {
        
        if( visible )
        {
            /**
             * code to execute if the sprite is rotated, scaled or if is
             * transparent
             */
            if( alpha < 1 || angle != 0 || xScale != 1 || yScale != 1 )
            {
            g.save();
               //to make sprite opaque
                g.setGlobalAlpha( alpha );

                g.translate( x + w/2  , y + h/2 );
                g.rotate( angle );
                g.scale( xScale, yScale );
                g.translate( -( x + w/2 ) , -( y + h/2 ) );

                drawLine( g );
            g.restore();
            }
            else
            {
             drawLine( g );  
            }
            
        }//visible
        
    }//

    
    private void drawLine( GraphicsContext g )
    {
    //setting lineColor & line width
    g.setStroke( lineColor );
    g.setLineWidth( lineWidth );

    g.beginPath();
    g.moveTo( x, y );
    g.lineTo( x + w , y + h );

    g.stroke();
    }
    
    
    public int getLineWidth() {
        return lineWidth;
    }

    public void setLineWidth(int lineWidth) {
        this.lineWidth = lineWidth;
    }

    public Color getLineColor() {
        return lineColor;
    }

    public void setLineColor(Color lineColor) {
        this.lineColor = lineColor;
    }

    @Override
    public void update(float l) 
    {}
    
    
}//class
