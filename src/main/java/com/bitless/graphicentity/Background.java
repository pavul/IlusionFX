package com.bitless.graphicentity;

import com.bitless.ntfc.Drawable;
import com.bitless.ntfc.Positionable;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;

/**
 * this can be used to draw big brackgrounds with images
 * or to draw tiles that does not collide with other entities
 * @author pavulzavala
 */
public class Background extends BaseBackground
                        implements Positionable, Drawable
{
   
    protected Image img;

    public Background( Image img ) 
    {
        this.x = 0;
        this.y = 0;
        this.w = 0;
        this.h = 0;
        
        if( img != null )
        {
        this.w = (int)img.getWidth();
        this.h = (int)img.getHeight();
        this.img = img;
        }
        
    }//

    
    /**
     * if you want to know width and height of background,
     * you can retrieve those values from image properties
     * @return 
     */
    public Image getImg() {
        return img;
    }

    /**
     * set new image to background, this image will
     * change width and height parameters
     * @param img 
     */
    public void setImg(Image img) 
    {
        this.w = (int)img.getWidth();
        this.h = (int)img.getHeight();
        this.img = img;
    }

    /**
     * this set position of X & Y of the background
     * @param x
     * @param y 
     */
    @Override
    public void setPosition( float x, float y) 
    {
        this.x = (int)x;
        this.y = (int)y;
    }//

    /**
     * this will draw background image in X & Y position
     * @param g 
     */
    @Override
    public void draw( GraphicsContext g ) 
    {
        g.drawImage( this.img, this.x, this.y );
    }//
    
     
}//
