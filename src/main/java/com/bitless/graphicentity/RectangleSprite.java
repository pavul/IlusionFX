
package com.bitless.graphicentity;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

/**
 *
 * @author PavulZavala
 */
public class RectangleSprite extends BaseSprite
{

    private Color rectangleColor;
    private boolean fillRectangle;
    
    public RectangleSprite( float x, float y, float w, float h )
    {
    super();
    this.x = x;
    this.y = y;
    this.w = w;
    this.h = h;
    rectangleColor = Color.BLACK;
    fillRectangle = true;
    }
    
    
    @Override
    public void draw(GraphicsContext g) 
    {
        
        if( visible )
        {
            /**
             * code to execute if the sprite is rotated, scaled or if is
             * transparent
             */
            if( alpha < 1 || angle != 0 || xScale != 1 || yScale != 1 )
            {
            g.save();
               //to make sprite opaque
                g.setGlobalAlpha( alpha );

                g.translate( x + w/2  , y + h/2 );
                g.rotate( angle );
                g.scale( xScale, yScale );
                g.translate( -( x + w/2 ) , -( y + h/2 ) );

                drawRectangle( g );
            g.restore();
            }
            else
            {
             drawRectangle( g );
            }
            
        }//
        
    }

    private void drawRectangle( GraphicsContext g )
    {
        if( fillRectangle )
        {
        g.setFill(rectangleColor);
        g.fillRect( x, y, w, h );
        }
        else
        {
        g.setStroke(rectangleColor);
        g.strokeRect( x, y, w, h );
        }
    }
    
    
    @Override
    public void update(float l) 
    {}

    public Color getRectangleColor() {
        return rectangleColor;
    }

    public void setRectangleColor(Color rectangleColor) {
        this.rectangleColor = rectangleColor;
    }

    public boolean isFillRectangle() {
        return fillRectangle;
    }

    public void setFillRectangle(boolean fillRectangle) {
        this.fillRectangle = fillRectangle;
    }

    
    
    
}
