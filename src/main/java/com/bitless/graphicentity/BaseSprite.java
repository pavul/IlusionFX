
package com.bitless.graphicentity;

import com.bitless.cfg.Config;
import com.bitless.ntfc.Drawable;
import com.bitless.ntfc.Moveable;
import com.bitless.ntfc.Positionable;
import com.bitless.ntfc.Updatable;

/**
 *
 * @author PavulZavala
 */
public abstract class BaseSprite 
        implements Drawable, Moveable, Updatable, Positionable
{

    /**
     * id variable starting point in 1000,
     * all sprites should have an id to get them
     * when necessary
     */
    protected int id;
    
    /**
     * label can be used to group several sprites, 
     * like enemies, items, powerups, etc.
     */
    protected String label;
    
    protected float x, y, w, h;
    
    protected float spdX; //sprite speed on X
    protected float spdY; //sprite speed on Y
    
    protected boolean visible; //if sprite is visible or not
    
    /**
     * this value can have values between 0 and 1
     * value of 0 means fully transparent
     * value of 1 means fully opaque
     * value of 0.5 means semitransparent
     */
    protected float alpha;//to make visible or translucient
    
     /**
     * this means what is the angle the sprite is pointing, default is 0
     * however grades are clockwise it means:
     * 0 is right,
     * 90 is down,
     * 180 is left,
     * 270 is up
     * when this value is different than 0, this sprite will be drawn
     * rotate to the angle value
     */
    protected float angle; //angle of sprite in degrees, default 0
    
    protected float xScale;//scale on X coord
    protected float yScale;//scale on Y coord

    
    public BaseSprite()
    {
    //id set after increasement
        id = ++Config.SPRITE_ID_COUNTER;
        visible = true;
        angle = 0;
        xScale = 1;
        yScale = 1;
        alpha = 1;
        label = ""; //by default label is empty
    }
    
    
    /**
     * move sprite in x axis
     * @param x 
     */
    @Override
    public void moveX(float x) 
    {
        this.x += x;
    }

    /**
     * move sprite in y axis
    */
    @Override
    public void moveY(float y) 
    {
        this.y += y;
    }

    /**
     * move sprite in both axis
     * @param x
     * @param y 
     * @Usage this is usage
     */
    @Override
    public void moveXY( float x, float y ) 
    {
        moveX( x );
        moveY( y );
    }

    /**
     * move sprite with spd on X axis
     */
    @Override
    public void moveXSpd() 
    {
        x += spdX;
    }

    
    /**
     * move sprite with spd on Y axis
     */
    @Override
    public void moveYSpd() 
    {
        y += spdY;
    }

      
    /**
     * set sprite X & Y position 
     * @param x
     * @param y 
     */
    @Override
    public void setPosition(float x, float y) 
    {
        this.x = x;
        this.y = y;
    }
    
     /**
     * id can be only returned due is set
     * only when the sprite is created
     * @return the id of the current Sprite
     */
    public int getId() {
        return id;
    }
    
        public void setLabel(String Label) {
        this.label = Label;
    }

    public String getLabel() {
        return label;
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    public float getW() {
        return w;
    }

    public void setW(float w) {
        this.w = w;
    }

    public float getH() {
        return h;
    }

    public void setH(float h) {
        this.h = h;
    }

    public float getSpdX() {
        return spdX;
    }

    public void setSpdX(float spdX) {
        this.spdX = spdX;
    }

    public float getSpdY() {
        return spdY;
    }

    public void setSpdY(float spdY) {
        this.spdY = spdY;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public float getAlpha() {
        return alpha;
    }

    public void setAlpha(float alpha) {
        this.alpha = alpha;
    }

    public float getAngle() {
        return angle;
    }

    public void setAngle(float angle) {
        this.angle = angle;
    }

    public float getxScale() {
        return xScale;
    }

    public void setxScale(float xScale) {
        this.xScale = xScale;
    }

    public float getyScale() {
        return yScale;
    }

    public void setyScale(float yScale) {
        this.yScale = yScale;
    }
    
    
    
    
}//
