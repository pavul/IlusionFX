package com.bitless.graphicentity;

import javafx.scene.image.Image;

/**
 * this class can represent a tile in position X & Y
   and the tileFrame value is the imageIndex of the image to 
   display, this can be used to positionate several sprites or
   * other objects in a grid, that's why there is an empty constructor
 * @author pavulzavala
 */
public class Tile extends Background
{
   
    protected int imageIndex;

    /**
     * constructor 1 emptry to use setters
     * @TODO check if this empty constructor is usable under certain circumstances
     */
    public Tile(  ) 
    {
    super( null );
    }

    /**
     * constructor 2 to set all parameters of a tile
     * @param x coord x
     * @param y coord y
     * @param index 
     * @param img 
     */
    public Tile( int x, int y, int index, Image img )
    {
    super( img );
    this.x = x;
    this.y = y;
    this.imageIndex = index;
    }//const

    public int getIndex() {
        return imageIndex;
    }

    public void setIndex(int index) {
        this.imageIndex = index;
    }

}//
