
package com.bitless.graphicentity.clip;

import javafx.geometry.Point2D;

/**
 * this class will create a circle figure
 * in order to set the path points for ClipScreen
 * @author PavulZavala
 */
public final class CircleClipFigure extends ClipFigure
{

    private float radius;
    
    public CircleClipFigure( Point2D point1,Point2D point2, float radius )
    {
       super( point1, point2 );
       this.radius = radius;
    }

    public float getRadius() {
        return radius;
    }
    
}//
