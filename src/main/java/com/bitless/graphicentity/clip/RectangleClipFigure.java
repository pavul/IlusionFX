
package com.bitless.graphicentity.clip;

import javafx.geometry.Point2D;

/**
 *
 * @author PavulZavala
 */
public class RectangleClipFigure extends ClipFigure
{
  private float width;
  private float height;
    
    public RectangleClipFigure( Point2D point1, Point2D point2, float width, float height )
    {
        super( point1, point2 );
        this.width = width;
        this.height = height;
    }

    public float getWidth() {
        return width;
    }

    public float getHeight() {
        return height;
    }

    
}//
