
package com.bitless.graphicentity.clip;

import com.bitless.graphicentity.Sprite;

/**
 * this class represent the click figure of an sprite,
 * when clipping an sprite we may choose to display an
 * squared or circle clip.
 * 
 * @author PavulZavala
 */
public final class SpriteClipFigure extends ClipFigure
{
    private Sprite sprite;

    private ClipShape shape;
    
    private float width;
    private float height;
    
    /**
     * this will create an Sprite figure for clipping,
     * even if the ClipShape is rectangular or circular, both
     * need width and height, in circle case will be radiusWidht
     * and radiusHeight
     * @param sprite
     * @param shape
     * @param width
     * @param height 
     */
    public SpriteClipFigure( Sprite sprite, ClipShape shape, float width, float height )
    {
    super( null,null );
    this.sprite = sprite;
    this.shape = shape;
    this.width = width;
    this.height =height;
    }

    public Sprite getSprite() {
        return sprite;
    }

    public ClipShape getShape() {
        return shape;
    }

    public float getWidth() {
        return width;
    }

    public float getHeight() {
        return height;
    }

    
}//
