
package com.bitless.graphicentity.clip;

/**
 * this enums contains all shapes
 * available to clip, usually when
 * it is needed to shape an sprite
 * when the screen is clipped
 * @author PavulZavala
 */
public enum ClipShape 
{
    TRIANGLE,
    RECTANGLE,
    CIRCLE,
    POLIGON;   
}//
