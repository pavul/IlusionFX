
package com.bitless.graphicentity.clip;

import javafx.geometry.Point2D;

/**
 * this class will be the base for all figures that
 * can be applicable to the clip screen, to clip a
 * figure we must set path points in GrapicsContext2d
 * all these points represent path points
 * @author PavulZavala
 */
public abstract class ClipFigure 
{
    protected Point2D point1;
    protected Point2D point2;

    
    public ClipFigure(Point2D point1,Point2D point2)
    {
    this.point1 = point1;
    this.point2 = point2;
    }

    public Point2D getPoint1() {
        return point1;
    }

    public Point2D getPoint2() {
        return point2;
    }
    
    
    
    
}//
