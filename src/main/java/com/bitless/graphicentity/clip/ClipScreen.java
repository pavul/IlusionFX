
package com.bitless.graphicentity.clip;

import com.bitless.ntfc.Drawable;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;

/**
 * this class clips the content of the view/screen or level
 * and it only let see a part ( like scoping )
 * @author PavulZavala
 */
public class ClipScreen implements Drawable 
{
    
    Paint paintColor;
    
    ClipFigure[] figures;
    
    public ClipScreen()
    {
    this.paintColor = Color.TRANSPARENT;
   
    }

    @Override
    public void draw(GraphicsContext g) 
    {
        g.setFill( paintColor );
        
        g.beginPath();
        
        //inside we have to paint all figures to clip
        //by default there will be circles
        for( int i = 0; i < figures.length; i++ )
        { 
            iterateFigure( g, figures[ i ] );
            g.closePath();
        }
        
        
        
        g.clip();
        
    }
    
    private void iterateFigure( GraphicsContext g, ClipFigure clipFigure )
    {
        
        if( clipFigure instanceof CircleClipFigure )
        {System.err.println("::: entrando al circle");
            CircleClipFigure ccf = (CircleClipFigure)clipFigure;
            clipCircle(g, ccf);
        }
        
        if( clipFigure instanceof RectangleClipFigure )
        {
            RectangleClipFigure rcf = (RectangleClipFigure)clipFigure;
            clipRectangle(g, rcf);
        }
        
        if( clipFigure instanceof TriangleClipFigure )
        {
            TriangleClipFigure tcf = (TriangleClipFigure)clipFigure;
            clipTriangle(g, tcf);
        }
        
        if( clipFigure instanceof SpriteClipFigure )
        {
            SpriteClipFigure scf = (SpriteClipFigure)clipFigure;
            clipSprite(g, scf);
        }
    
        
    }
    
private void clipTriangle(GraphicsContext g, TriangleClipFigure figure )
{
    g.moveTo( figure.getPoint1().getX(), figure.getPoint1().getY() );
    g.moveTo( figure.getPoint2().getX(), figure.getPoint2().getY() );
    g.moveTo( figure.getPoint3().getX(), figure.getPoint3().getY() );
}

private void clipRectangle(GraphicsContext g, RectangleClipFigure figure )
{
    g.rect( figure.getPoint1().getX(), figure.getPoint1().getY(),
            figure.getWidth(), figure.getHeight() );
}

private void clipCircle(GraphicsContext g, CircleClipFigure figure )
{
    //this will set the circle, from center defined by point1
    //from 0 until 360 angle ( full circle )
    g.arc( figure.getPoint1().getX() , figure.getPoint1().getY(),
            figure.getRadius(), figure.getRadius(), 0, 360 );    
}
    
/**
 * for now the sprite will be only drawn in circle form
 * @param g
 * @param figure 
 */
private void clipSprite( GraphicsContext g, SpriteClipFigure figure  )
{
    
    switch( figure.getShape() )
    {
        case CIRCLE:
//            CircleClipFigure ccf = (CircleClipFigure)figure.getFigureToDraw();
            //this will draw the circle surrounding the sprite, depending
            //on the radius value and will 
             g.arc( figure.getSprite().getX() + figure.getSprite().getCenterX(),
                    figure.getSprite().getY() + figure.getSprite().getCenterY(),
                    figure.getWidth(), figure.getHeight(),
                    0, 360);
            
            break;
        case RECTANGLE:
           
            g.rect( figure.getSprite().getX() + figure.getSprite().getCenterX() - figure.getWidth()/2, 
                    figure.getSprite().getY() + figure.getSprite().getCenterY() - figure.getHeight()/2,
                    figure.getWidth(), figure.getHeight() );
            
            break;
//NO TRIANGLE IMPLEMENTATION FOR NOW
//        case TRIANGLE:
//            TriangleClipFigure tcf = (TriangleClipFigure)figure.getFigureToDraw();
//            clipTriangle(g, tcf );
//            break;
    }
    
    
}

    public void setPaintColor(Paint paintColor) {
        this.paintColor = paintColor;
    }

    public void setFigures(ClipFigure[] figures) {
        this.figures = figures;
    }


    
}//
