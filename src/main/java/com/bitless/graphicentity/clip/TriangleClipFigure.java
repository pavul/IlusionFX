
package com.bitless.graphicentity.clip;

import javafx.geometry.Point2D;

/**
 * this shape will represent a triangle,
 * every point will be added to the path
 * @author PavulZavala
 */
public final class TriangleClipFigure extends ClipFigure
{
    private Point2D point3;
    
    public TriangleClipFigure( Point2D point1,Point2D point2,Point2D point3 )
    {
        super( point1, point2 );
    this.point3 = point3;
    }

    public Point2D getPoint3() {
        return point3;
    }
    
}//
