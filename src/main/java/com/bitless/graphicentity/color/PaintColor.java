
package com.bitless.graphicentity.color;

/**
 * this will represent a set of colors that 
 * are used to change the paint color of an sprite
 * ( those are the values of HUE values when a ColorAdjustment color 
 * wants to be created )
 * <b>
 * NOTE: the range of these values are from -1 to 1
 * </b>
 * @author PavulZavala
 */
public enum PaintColor 
{
    HUE_STRONGBLUE( -0.8f ), 
    HUE_BLUE( -0.7f ),
    HUE_PURPLE( -0.6f ),
    HUE_VIOLET( -0.5f ),
    HUE_MAGENTA( -0.4f ),
    HUE_LIGHTRED( -0.3f ),
    HUE_RED( -0.2f ),
    HUE_ORANGE( -0.1f ),
    HUE_YELLOW( 0.1f ),
    HUE_LIGHTYELLOW( 0.2f ),
    HUE_LIGHTGREEN( 0.3f ),
    HUE_GREEN( 0.4f ),
    HUE_GREEN2( 0.5f ),
    HUE_AQUA( 0.6f ),
    HUE_INDIGO( 0.7f ),
    HUE_LIGHTBLUE( 0.8f ),
    HUE_BLUE2( 0.9f ),
    HUE_STRONGBLUE2( 1.0f ),
    HUE_WHITE( 0.0f ),
    HUE_BLACK( 0.0f );
    
    private final float paintColorValue;
    PaintColor( float paintColorValue )
    {
        this.paintColorValue = paintColorValue;
    }

    public float getPaintColorValue() {
        return this.paintColorValue;
    }
    
    
}//
