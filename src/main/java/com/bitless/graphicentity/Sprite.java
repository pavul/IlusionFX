package com.bitless.graphicentity;

import com.bitless.cfg.Animation;
import com.bitless.cfg.Config;
import com.bitless.graphicentity.color.PaintColor;
import com.bitless.ntfc.Animationable;
import com.bitless.ntfc.Centerable;
import com.bitless.ntfc.ColorAdjustable;
import com.bitless.ntfc.Glowable;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.effect.ColorAdjust;
import javafx.scene.effect.Glow;
import javafx.scene.image.Image;

/**
 * graphic entity in used to display several images as frames usually
 * if more functionality is needed you can extend this class, 
 */
public class Sprite extends BaseSprite
    implements   Centerable, Animationable, ColorAdjustable, Glowable
{
    
   
    protected boolean animEnd; //if animation has been reashed last frame
    
    //    protected Collider collider; //collision object of this sprite

    protected Animation loop;
    
    /**
     *frames of this animation
     */
    protected Image[ ] frames;
    /**
     * current drawn frame
     */
    protected int frameIndex;
    /**
     * last frame to start animation over again
     */
    protected int lastFrame;
    /**
     * step counter
     */
    protected int animStep;
    /**
     * steps to next frame
     */
    protected int animStepLimit;
    
    
    /**
     *
     * below variables are useful for ColorAdjust effect
       where the sprite color will change by the specified
       color of HSB.
       * Acceptable value rage are between -1 to 1
       * by default value will be -5, means ColorAdjust is disabled
     */
    private float hueAdjust = -5f; //this can be from -1 to 1
    
    /**
     * this variable will be set to graphics object to
     * change the sprite color
     */
    private ColorAdjust hsbColor;
//            new ColorAdjust( currentAdjust, 1, 0, 0 );
    
    //current level of the glow, by default 0
    private float glowLevel;
    
    //the glow effect object
    private Glow glow;
    
    /**
     * this create the sprite specifying the sprite frames or frame of the initial animation
     * @param frames
     */
    public Sprite( Image... frames )
    {
        super();
        
        this.frames = frames;
        x = 0;
        y = 0;
        w = ( float )frames[ 0 ].getWidth();
        h = ( float )frames[ 0 ].getHeight();
        frameIndex = frames.length - 1;
        lastFrame = frames.length -1;
    
        loop = Animation.FOWARD;
        animStepLimit = 10;
         
        hsbColor = new ColorAdjust( 0, 1, 0, 0 );
         
        glowLevel = 0;
        glow = new Glow( glowLevel );
        glow.setLevel( glowLevel );
    }//
    
    
    
  
    
    
    /**
     * this will draw sprite current frame only if visible is true
     * if we have angle, sprite is scaled and/or alpha is not 1, 
     * then this sprite will be displaying those changes
     * @param g 
     */
    @Override
    public void draw(GraphicsContext g) 
    {
        if( visible )
        {
            
            updateAnimation();
            
            /**
             * code to execute if the sprite is rotated, scaled or if is
             * transparent
             */
//            if( alpha < 1 || angle != 0 || xScale != 1 || yScale != 1 )
//            {
            g.save();
            
                //to make sprite opaque or transparent
                if( alpha < 1 )
                    g.setGlobalAlpha( alpha );

                
                //to shrink, expand or rotate
                if( angle != 0 || xScale != 1 || yScale != 1 )
                {
                    g.translate( x + w/2  , y + h/2 );
                    g.rotate( angle );
                    g.scale( xScale, yScale );
                    g.translate( -( x + w/2 ) , -( y + h/2 ) );
                }
                
                //if hueAdjust != -5f means colorAdjust must be applied
                //if so, we set again the hsbColor to the graphics context
                if( hueAdjust != Config.COLOR_ADJUST_DISABLED )
                {
//                    g.applyEffect(hsbColor );
                    g.setEffect( hsbColor );
                }
                  
                if( glowLevel != 0.3 )
                {
//                    g.applyEffect(glow);
                    g.setEffect( glow );
                }
                
                g.drawImage( frames[ frameIndex ], x, y );
            g.restore();
//            }
//            else
//            {
////                g.save();
////                g.drawImage( frames[ frameIndex ], x, y );
////                g.restore();
////            //code to execute if normal sprite(Image) is displayed
//            g.drawImage( frames[ frameIndex ], x, y );
//            }
            
        }//
       
    }//

   
    @Override
    public void update( float l ) 
    {
    }//update

    
    /**
     *getters & setterw 
     */
    
    /**
     * get current alpha value of this sprite
     * @return 
     */
    @Override
    public float getAlpha() {
        return alpha;
    }

    /**
     * this value can have values between 0 and 1
     * value of 0 means fully transparent
     * value of 1 means fully opaque
     * value of 0.5 means semitransparent
     * @param alpha
     */
    @Override
    public void setAlpha(float alpha) {
        this.alpha = alpha;
    }

    /**
     * this means what is the angle the sprite is pointing, default is 0
     * however grades are clockwise it means:<br />
     * 0 is right,<br />
     * 90 is down,<br />
     * 180 is left,<br />
     * 270 is up<br />
     * when this value is different than 0, this sprite will be drawn
     * rotate to the angle value
     * @param angle
     */
    @Override
    public void setAngle(float angle) {
        this.angle = angle;
    }

    /**
     * get the current angle where the sprite is pointing
     * @return 
     */
    @Override
    public float getAngle() {
        return angle;
    }

    @Override
    public void setyScale(float yScale) {
        this.yScale = yScale;
    }

    @Override
    public float getyScale() {
        return yScale;
    }

    @Override
    public void setxScale(float xScale) {
        this.xScale = xScale;
    }

    public float getxScale() {
        return xScale;
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    public float getW() {
        return w;
    }

    public void setW(float w) {
        this.w = w;
    }

    public float getH() {
        return h;
    }

    public void setH(float h) {
        this.h = h;
    }

    public float getSpdX() {
        return spdX;
    }

    public void setSpdX(float spdX) {
        this.spdX = spdX;
    }

    public float getSpdY() {
        return spdY;
    }

    public void setSpdY(float spdY) {
        this.spdY = spdY;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public boolean isAnimEnd() {
        return animEnd;
    }

    public void setAnimEnd(boolean animEnd) {
        this.animEnd = animEnd;
    }

    public int getFrameIndex() {
        return frameIndex;
    }

    public void setFrameIndex(int frameIndex) {
        this.frameIndex = frameIndex;
    }

    /**
     * return the last frame of the current frames (animation)
     * NOTE: there is no point to set last frame
     * @return 
     */
    public int getLastFrame() {
        return lastFrame;
    }

    public Image[] getFrames() {
        return frames;
    }

    /**
     * method used to set a new animation images to the sprite
     * <b>
     * Note: this set width and height for the new image in position index 0
     * </b>
     * @param frames 
     */
    public void setFrames(Image[] frames) 
    {
        this.frames = frames;
        this.w = (float)frames[ 0 ].getWidth();
        this.h = (float)frames[ 0 ].getHeight();
    }

    public int getAnimStepLimit() {
        return animStepLimit;
    }

    public void setAnimStepLimit(int animStepLimit) {
        this.animStepLimit = animStepLimit;
    }

   
    /**
     * get current loop of animation used by this sprite
     * @return 
     */
    public Animation getLoop() {
        return loop;
    }

    /**
    * set a new animation loop, 
    * default is: Animation.FOWARD,
    * others available are:
    *Animation.BACKWARD,
    *Animation.STOPATEND
    * <b>@note: if those don't work for you, 
    * you can extend this class and override draw method
    * with the implementation best suit your needs</b>
    * @param loop 
    */
    public void setLoop(Animation loop) 
    {
        this.loop = loop; 
    }//

    /**
     * gets the center of the width of the sprite, 
     * if sprite width is 32 this will return 16,
     * <b>width/2 it also taken as radius for circle collisions</b>
     * @return the center of the width of the sprite
     */
    @Override
    public float getCenterX() 
    {
    return this.w/2;
    }

    /**
     * gets the center of the height of the sprite, 
     * if sprite height is 32 this will return 16
     * @return the center of the height of the sprite
     */
    @Override
    public float getCenterY() 
    {
    return this.h/2;
    }

    
     /**@TODO check animation end for different animations loops
     * this function changes the frames of the sprite, this should be used on 
     * update method of the level, if this is not called there animation wont change
     * frames, it will loop through Animation enum value
     */
    @Override
    public void updateAnimation() 
    {
     //if the sprite have only 1 frame,
        //increasment of frameIndex wont be updated
        if( frames.length <= 1 ) return;
       
            animStep++;
            
            if( animStep >= animStepLimit )
            {
                animStep = 0;
            
                switch( loop )
                {

                    case FOWARD:
                        frameIndex++;
                        if( frameIndex > lastFrame )
                        {
                        frameIndex = 0;
                        animEnd = true;
                        }
                        break;
                        
                    case BACKWARD:
                        frameIndex--;
                        if( frameIndex < 0 )
                        {
                        frameIndex = lastFrame;
                        animEnd = false;
                        }
                        break;

                    default://stop at end
                        if(frameIndex < lastFrame)
                        {  frameIndex++;}
                        else if( frameIndex == lastFrame )
                        {
                          frameIndex = 0;
                          animEnd = true;
                        }
                          
                        break;
                }//suich
                
            }//
            
    }

    /**
     * this will set the HSB colorAdjust of the sprite
     * for some predefined colors, if white or black color are set
     * this will change the brigthnes of the sprite instead the HUE value
     * @param colorToSet 
     */
    @Override
    public void setColor( PaintColor colorToSet ) 
    {
        this.hueAdjust = colorToSet.getPaintColorValue();
               
        switch( colorToSet )
        {
            case HUE_WHITE:
                this.hsbColor.setBrightness( 1 );
                break;
            case HUE_BLACK:
                this.hsbColor.setBrightness( -1 );
                break;
            default:
                this.hsbColor.setHue( this.hueAdjust );
                break;
        
        }
        
//        if( PaintColor.HUE_WHITE == colorToSet )
//        {
//            
//        }
//        else if( PaintColor.HUE_BLACK == colorToSet )
//        {
//            
//        }
//        else
//        {
//            
//        }
            
    }//

    /**
     * this method will set the new ColorAdjust, it means
     * that Hue, Saturation, Brigthness & contrast can be set
     * to the sprite directlty
     * @param colorToSet 
     */
    @Override
    public void setColor( ColorAdjust colorToSet ) 
    { 
        //change hueAdjust value in order to 
        //execute hsbColor in draw method
        this.hueAdjust = 0;
        this.hsbColor = colorToSet;
    }

    /**
     * if color adjust was applied this method will 
     * set a new ColorAdjust Object to set the defaults
     * settings hence the sprite will be restored
     */
    @Override
    public void restoreColor() 
    {
        this.hueAdjust = Config.COLOR_ADJUST_DISABLED;
        this.hsbColor = new ColorAdjust();
    }

    /**
     * this will set the new level of the glow
     */
    @Override
    public void setGlowLevel( float newGlowLevel ) 
    {
        this.glowLevel = newGlowLevel;
        this.glow.setLevel( glowLevel );
    }

    /**
     * this will set the glowLevel to it default 
     * level of 0.3f
     */
    @Override
    public void restoreGlow() 
    {
        setGlowLevel( 0.3f );
    }

}//class
