
package com.bitless.task;

/**
 * this class will execute a task in certain amount of steps,
 * the implementation to execute must be set in the level 
 * @author PavulZavala
 */
public class Task
{

    protected boolean active;
    protected int counter;
    
    
    
    public Task()
    {
    active = true;
    counter = 0;
    }

    /**
     * this task must be called in update method
     * it will decrease counter until it comes to 0
     * when it reach 0 ex.execute() method will be triggered
     * and active will be set to false,
     * to activate it again you must set counter to some value in
     * order to execute this method again ( setCounter( someValue ) )
     * @param ex 
     */
    public void processTask( Excecutable ex )
    {
        counter--;
        if( counter < 1 && active )
        {
            counter = 0;
            active = false;
            ex.execute();
        }
        
    }//


    
    /**
     * getters & setters
     * @return 
     */
        public int getCounter() {
        return counter;
    }

    public void setCounter(int counter) 
    {
        this.counter = counter;
        this.active = true;
    }
    
    
}//
