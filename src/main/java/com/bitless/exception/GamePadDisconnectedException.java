
package com.bitless.exception;

/**
 * this exception will be thrown when a gamepad
 * device is disconnected
 * @author PavulZavala
 */
public class GamePadDisconnectedException extends Exception
{}//
